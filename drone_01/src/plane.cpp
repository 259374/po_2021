#include <iostream>
#include <iterator>

#include "coor_system.hh"
#include "vector.hh"
#include "plane.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

const Vector<3> & Plane::operator [] (int indx) const{
  if (indx < 0 || indx > 3){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}

void Plane::draw(drawNS::Draw3DAPI *drafter){
  std::vector<std::vector<drawNS::Point3D>> vtmp;
  std::vector<drawNS::Point3D> top;
  std::vector<drawNS::Point3D> side1;
  std::vector<drawNS::Point3D> side2;
  double step = (((*this)[1]-(*this)[0]).length()) / 2;

  //Top
  top.push_back(convert((*this)[0]));
  top.push_back(convert((*this)[1]));
  top.push_back(convert((*this)[2]));
  top.push_back(convert((*this)[3]));
  top.push_back(convert((*this)[0]));
  vtmp.push_back(top);

  side1.push_back(convert((*this)[0]+Vector<3>({0,step,0})));
  side1.push_back(convert((*this)[3]+Vector<3>({0,step,0})));
  vtmp.push_back(side1);

  side2.push_back(convert((*this)[1]+Vector<3>({step,0,0})));
  side2.push_back(convert((*this)[2]+Vector<3>({step,0,0})));
  vtmp.push_back(side2);

  this->idx = drafter -> draw_polyhedron(vtmp, "green");
}
