#include <iostream>
#include <iterator>

#include "drone.hh"
#include "coor_system.hh"
#include "hex_prism.hh"
#include "vector.hh"
#include "cuboid.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

void Drone::draw(drawNS::Draw3DAPI *drafter){
  body.draw(drafter);
  for (int i=0; i<4; i++){
    (propellers[i]).draw(drafter);
  }
}

void Drone::goForward(double distance){
  this->move(Vector<3>({distance, 0, 0}));
}
void Drone::goUp(double distance){
  this->move(Vector<3>({0, 0, distance}));
}
void Drone::goSide(double distance){
  this->move(Vector<3>({0, distance, 0}));
}

void Drone::turn(RotationMatrix<3> rot){
  this->rotate(rot);
}

void Drone::clearShape(drawNS::Draw3DAPI *drafter) const{
  drafter->erase_shape((this->getBody()).getIdx());
  for (int i=0; i<4; i++){
    drafter->erase_shape((this->getPropeller(i)).getIdx());
  }
}
