#include <iostream>

#include "drone.hh"
#include "vector.hh"
#include "plane.hh"
#include "coor_system.hh"
#include "cuboid.hh"
#include "hex_prism.hh"
#include "../Dr3D_gnuplot_api.hh"

using std::vector;
using drawNS::Point3D;
using drawNS::APIGnuPlot3D;
using std::cout;
using std::cin;
using std::cerr;
using std::endl;

void wait4key() {
  do {
    cout << "\nPress a key to continue..." << endl;
  } while(cin.get() != '\n');
}

void info(Drone &test){
  cout << endl << "Center: " << test.getcenter() << endl;
  cout << "Orientation: " << endl << test.getorient() << endl;
}

int main(){
  Vector<3> axes_center({0,0,0});
  Vector<3> z_axis({0,0,1}), y_axis({0,1,0}), x_axis({1,0,0});
  RotationMatrix<3> identity_matrix(0, z_axis);
  Drone test_drone(axes_center, identity_matrix, nullptr);
  Plane ground(axes_center, identity_matrix, nullptr, -5);
  Vector<3> mv_vec = axes_center;
  Vector<3> rot_vec;
  RotationMatrix<3> rot = identity_matrix;
  double angle_degrees;
  double multiplier;
  char axis;
  std::string opt = "";

  drawNS::Draw3DAPI * drafter = new APIGnuPlot3D(-20,20,-20,20,-10,10,0);
  info(test_drone);
  test_drone.draw(drafter);
  ground.draw(drafter);

  cout.precision(5);

  while(opt[0] != 'k'){
  cout << "Wybierz opcje (m - menu) : ";
  cin >> opt;
  switch(opt[0]){
    case 'm':{
      cout << "\no - obrot o zadany kat" << endl;
      cout << "p - przesuniecie o zadany wektor" << endl;
      cout << "m - wyswietl menu " << endl;
      cout << "r - wyswietl macierz rotacji" << endl;
      cout << "k - koniec programu" << endl;
    }
    break;
    case 'p':{
      cout << endl << "Podaj wspolrzedne wektora przesuniecia : ";
      cin >> mv_vec;
      test_drone.goForward(mv_vec[0]);
      test_drone.goSide(mv_vec[1]);
      test_drone.goUp(mv_vec[2]);
      info(test_drone);
      test_drone.clearShape(drafter);
      test_drone.draw(drafter);
    }
    break;
    case 'o':{
      cout << endl << "Podaj kat obrotu (w stopniach) : ";
      cin >> angle_degrees;
      cout << "Podaj ilosc powtorzen operacji : ";
      cin >> multiplier;
      angle_degrees *= multiplier;
      cout << "Podaj os obrotu (x, y ,z): ";
      cin >> axis;
      switch(axis){
        case 'x': rot_vec = x_axis;
        break;
        case 'y': rot_vec = y_axis;
        break;
        case 'z': rot_vec = z_axis;
        break;
        default: cout << "Brak podanej osi" << endl;
        break;
      }
      rot = RotationMatrix<3>(angle_degrees, rot_vec);
      test_drone.turn(rot);
      info(test_drone);
      test_drone.clearShape(drafter);
      test_drone.draw(drafter);
    }
    break;
    case 'r':{
      cout << endl << rot;
    }
    break;
    case 'k': break;
    default:
      cout << "Wybierz jedna z opcji z menu" << endl;
    break;

  }
  wait4key();
  }

  delete drafter;
}
