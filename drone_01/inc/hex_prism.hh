#ifndef HEX_PRISM
#define HEX_PRISM

#include <iostream>
#include <cmath>
#include <array>

#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class HexagonalPrism : public CoorSystem{
  std::array<Vector<3>, 12> corners;
  double _height;
  double _side;
  int idx; //for drawing
public:
  HexagonalPrism() = default;
  HexagonalPrism(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
                 double height, double side):
	CoorSystem(position, orientation, parent), _height(height), _side(side), idx(-1) {
    double step = (_side)/sqrt(2);
    //Top
    corners[0] = Vector<3>({(_side/2 + step), (_side/2), (_height/2)});
    corners[1] = Vector<3>({0, (_side)/2 + step, (_height/2)});
    corners[2] = Vector<3>({(-1)*(_side/2 + step), (_side/2), (_height/2)});
    corners[3] = Vector<3>({(-1)*(_side/2 + step), (-1)*(_side/2), (_height/2)});
    corners[4] = Vector<3>({0, (-1)*((_side/2) + step), (_height/2)});
    corners[5] = Vector<3>({(_side/2 + step), (-1)*(_side/2), (_height/2)});

    //Bottom
    corners[6] = Vector<3>({(_side/2 + step), (_side/2), (-1)*(_height/2)});
    corners[7] = Vector<3>({0, (_side/2 + step), (-1)*(_height/2)});
    corners[8] = Vector<3>({(-1)*(_side/2 + step), (_side/2), (-1)*(_height/2)});
    corners[9] = Vector<3>({(-1)*(_side/2 + step), (-1)*(_side/2), (-1)*(_height/2)});
    corners[10] = Vector<3>({0, (-1)*((_side/2) + step), (-1)*(_height/2)});
    corners[11] = Vector<3>({(_side/2 + step), (-1)*(_side/2), (-1)*(_height/2)});
  };
  int getIdx() const {return idx;};
  void draw(drawNS::Draw3DAPI *drawing);

  const Vector<3> & operator [] (int indx) const;
};

#endif
