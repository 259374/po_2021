#ifndef PLANE_HH
#define PLANE_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class Plane : public CoorSystem{
  std::array<Vector<3>, 4> corners;
  double _height;
  int idx; //for drawing
public:
  Plane() = default;
  Plane(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height):
	CoorSystem(position, orientation, parent), _height(height), idx(-1) {
    corners[0] = Vector<3>({-20,-20,height});
    corners[1] = Vector<3>({-20,20,height});
    corners[2] = Vector<3>({20,20,height});
    corners[3] = Vector<3>({20,-20,height});
  };

  int getIdx() const {return idx;};
  void draw(drawNS::Draw3DAPI *drawing);
  const Vector<3> & operator [] (int indx) const;
};

#endif
