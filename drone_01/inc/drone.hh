#ifndef DRONE_HH
#define DRONE_HH

#include <iostream>
#include <cmath>
#include <array>

#include "vector.hh"
#include "coor_system.hh"
#include "cuboid.hh"
#include "hex_prism.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class Drone : protected CoorSystem{
  Cuboid body;
  std::array<HexagonalPrism, 4> propellers;
public:
  Drone() = default;
  Drone(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent):
  CoorSystem(position, orientation, parent),
  body(Vector<3>({0,0,0}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 2, 2, 4),
  propellers({
    HexagonalPrism(Vector<3> ({-1,2,1}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.5),
    HexagonalPrism(Vector<3> ({-1,-2,1}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.5),
    HexagonalPrism(Vector<3> ({1,2,1}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.5),
    HexagonalPrism(Vector<3> ({1,-2,1}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.5)
  }) {};
  void draw(drawNS::Draw3DAPI *drawing);

  void goForward(double distance);
  void goUp(double distance);
  void goSide(double distance);

  void turn(RotationMatrix<3> rot);

  void clearShape(drawNS::Draw3DAPI *drafter) const;

  Vector<3> getcenter() const {return getPosition();};
  RotationMatrix<3> getorient() const {return getOrient();};

  Cuboid getBody() const {return body;};
  HexagonalPrism getPropeller(int which_one) const {return propellers[which_one];};
};

#endif
