#ifndef COOR_SYS
#define COOR_SYS

#include <iostream>
#include <array>
#include "../Dr3D_gnuplot_api.hh"

#include "vector.hh"

class CoorSystem{
protected:
  CoorSystem *_parent;
  Vector<3> _center;
  RotationMatrix<3> _orientation;
public:
  //Constructors
  CoorSystem() = default;
  CoorSystem(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent = nullptr) :
  _parent(parent), _center(position), _orientation(orientation) {};

  void move(Vector<3> v) {_center = _center + v;};
  void rotate(RotationMatrix<3> mtrx) {_orientation = _orientation * mtrx;};
  Vector<3> calculate_point(Vector<3> local_point) const;
  CoorSystem calculate_from_parent(const CoorSystem & parent);

  Vector<3> getPosition() const {return _center;};
  RotationMatrix<3> getOrient() const {return _orientation;};
  CoorSystem* getParent() const {return _parent;};
};

drawNS::Point3D convert (Vector<3> vec);

#endif
