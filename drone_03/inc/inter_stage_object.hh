#ifndef INTER_STAGE_OBJECT_HH
#define INTER_STAGE_OBJECT_HH

#include <iostream>
#include <array>

#include "inter_drone.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class InterfaceStageObject{
public:
  virtual bool isAbove(InterfaceDrone* drone) = 0;
  virtual bool isLandingPossible(InterfaceDrone* drone) = 0;
  virtual Vector<3> getcenter() = 0;
};

#endif
