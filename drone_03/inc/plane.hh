#ifndef PLANE_HH
#define PLANE_HH

#include <iostream>
#include <array>

#include "inter_drawing.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class Plane : public InterfaceDrawing, public CoorSystem{
  std::array<Vector<3>, 4> corners;
  double _height;
public:
  Plane() = default;
  Plane(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height,  drawNS::Draw3DAPI* drafter):
	InterfaceDrawing(drafter, -1), CoorSystem(position, orientation, parent), _height(height){
    corners[0] = Vector<3>({-20,-20,height});
    corners[1] = Vector<3>({-20,20,height});
    corners[2] = Vector<3>({20,20,height});
    corners[3] = Vector<3>({20,-20,height});
  };

  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  const Vector<3> & operator [] (int indx) const;

};

#endif
