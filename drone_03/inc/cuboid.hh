#ifndef CUBOID_HH
#define CUBOID_HH

#include <iostream>
#include <array>

#include "inter_drawing.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class Cuboid : public InterfaceDrawing, public CoorSystem{
  std::array<Vector<3>, 8> corners;
  double _height;
  double _width;
  double _depth;
public:
  Cuboid() = default;
  Cuboid(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height, double width, double depth, drawNS::Draw3DAPI* drafter):
	InterfaceDrawing(drafter, -1), CoorSystem(position, orientation, parent), _height(height), _width(width), _depth(depth){
    //Top
    corners[0] = Vector<3>({(_width/2),(_depth/2),(_height/2)});
    corners[1] = Vector<3>({(_width/2),(-1)*(_depth/2),(_height/2)});
    corners[2] = Vector<3>({(-1)*(_width/2),(-1)*(_depth/2),(_height/2)});
    corners[3] = Vector<3>({(-1)*(_width/2),(_depth/2),(_height/2)});

    //Bottom
    corners[4] = Vector<3>({(_width/2),(_depth/2),(-1)*(_height/2)});
    corners[5] = Vector<3>({(_width/2),(-1)*(_depth/2),(-1)*(_height/2)});
    corners[6] = Vector<3>({(-1)*(_width/2),(-1)*(_depth/2),(-1)*(_height/2)});
    corners[7] = Vector<3>({(-1)*(_width/2),(_depth/2),(-1)*(_height/2)});
  };
  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  const Vector<3> & operator [] (int indx) const;
};

#endif
