#ifndef PLATEAU_HH
#define PLATEAU_HH

#include <iostream>
#include <array>
#include <random>
#include <cmath>

#include "inter_drawing.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24
#define M_PI 3.14159265358979323846

class Plateau : public InterfaceDrawing, public InterfaceStageObject, public CoorSystem{
  std::array<Vector<3>, 18> corners;
  double _height;
  double _size_min;
  double _size_max;
  int _number_of_corners;
public:
  Plateau() = default;
  Plateau(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height, double size_min, double size_max, drawNS::Draw3DAPI* drafter):
	InterfaceDrawing(drafter, -1), CoorSystem(position, orientation, parent), _height(height), _size_min(size_min), _size_max(size_max){
    double x_coor, y_coor, theta, radius, angle_deg;
    std::random_device rd;
    std::mt19937 generator(rd());
    std::mt19937 generator_size(rd());
    std::uniform_int_distribution<int> corners_distribution(3,9);
    std::uniform_int_distribution<int> size_distribution(_size_min, _size_max);

    _number_of_corners = corners_distribution(generator);

    angle_deg = 360 / _number_of_corners;
    theta = M_PI * (angle_deg / 180);

    for (int i=0; i<_number_of_corners; i++){
      radius = size_distribution(generator_size);
      x_coor = cos(i*theta) * radius;
      y_coor = sin(i*theta) * radius;
      corners[i] = Vector<3>({x_coor, y_coor, 0});
      corners[i+9] = Vector<3>({x_coor, y_coor, _height});
    }
  };

  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  virtual Vector<3> getcenter() override {return getPosition();};

  const Vector<3> & operator [] (int indx) const;

  virtual bool isAbove(InterfaceDrone* drone) override {return true;};
  virtual bool isLandingPossible(InterfaceDrone* drone) override {return true;};
};

#endif
