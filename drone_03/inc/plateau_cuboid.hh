#ifndef PLATEAU_CUBOID_HH
#define PLATEAU_CUBOID_HH

#include <iostream>
#include <array>

#include "inter_drone.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "cuboid.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class PlateauCuboid : public InterfaceStageObject, public Cuboid{
  std::array<Vector<3>, 8> corners;
  double _height;
  double _width;
  double _depth;
public:
  PlateauCuboid() = default;
  PlateauCuboid(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height, double width, double depth, drawNS::Draw3DAPI* drafter):
	Cuboid(position, orientation, parent, height, width, depth, drafter) {};

  virtual Vector<3> getcenter() override {return getPosition();};

  virtual bool isAbove(InterfaceDrone* drone) override {return true;};
  virtual bool isLandingPossible(InterfaceDrone* drone) override {return true;};
};

#endif
