#ifndef DRONE_HH
#define DRONE_HH

#include <iostream>
#include <cmath>
#include <array>
#include <thread>
#include <chrono>

#include "inter_drawing.hh"
#include "inter_drone.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "cuboid.hh"
#include "hex_prism.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class Drone : public InterfaceDrawing, public InterfaceStageObject, public InterfaceDrone, protected CoorSystem{
  Cuboid body;
  std::array<HexagonalPrism, 4> propellers;
public:
  Drone() = default;
  Drone(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,  drawNS::Draw3DAPI* drafter):
  InterfaceDrawing(drafter, -1),
  CoorSystem(position, orientation, parent),
  body(Vector<3>({0,0,0}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 1, 2, 4, drafter),
  propellers({
    HexagonalPrism(Vector<3> ({-1,2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter),
    HexagonalPrism(Vector<3> ({-1,-2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter),
    HexagonalPrism(Vector<3> ({1,2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter),
    HexagonalPrism(Vector<3> ({1,-2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter)
  }) {};

  virtual void goForward(double distance) override;
  virtual void goUp(double distance) override;
  virtual void goSide(double distance) override;
  virtual void turn(RotationMatrix<3> rot) override;

  virtual void turnPropellers() override;
  void animate(double distanceForward, double distanceSide, double distanceUp, drawNS::Draw3DAPI *drafter);

  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  virtual bool isAbove(InterfaceDrone* drone) override {return true;};
  virtual bool isLandingPossible(InterfaceDrone* drone) override {return false;};

  //Get
  virtual Vector<3> getcenter() override {return getPosition();};
  virtual Vector<3> dronegetcenter() override {return getPosition();};
  RotationMatrix<3> getorient() const {return getOrient();};

  Cuboid getBody() const {return body;};
  HexagonalPrism getPropeller(int which_one) const {return propellers[which_one];};
};

#endif
