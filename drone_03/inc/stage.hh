#ifndef STAGE_HH
#define STAGE_HH

#include <iostream>
#include <array>
#include <random>
#include <cmath>

#include "inter_drone.hh"
#include "inter_drawing.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

class Stage{
  std::vector<std::shared_ptr<InterfaceDrone>> drone_list;
  std::vector<std::shared_ptr<InterfaceStageObject>> object_list;
  std::vector<std::shared_ptr<InterfaceDrawing>> drawable_list;
  std::shared_ptr<InterfaceDrone> selected_drone;
  drawNS::Draw3DAPI* _drafter;
public:
  Stage(drawNS::Draw3DAPI* drafter) : _drafter(drafter) {};
  void animate(double distanceForward, double distanceSide, double distanceUp, RotationMatrix<3> rot);
  void drawEverything();
  void addStageObject();
  void deleteStageObject(unsigned int id);
  void showAllStageObjects();
  void addDrone();
  void deleteDrone(unsigned int id);
  void showAllDrones();
  void selectDrone(unsigned int id);

  void setSelectedDrone(std::shared_ptr<InterfaceDrone> droneToBeSelected) {selected_drone = droneToBeSelected;}
};

#endif
