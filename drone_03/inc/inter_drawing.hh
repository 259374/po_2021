#ifndef INTER_DRAWING_HH
#define INTER_DRAWING_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "../Dr3D_gnuplot_api.hh"

class InterfaceDrawing{
protected:
  drawNS::Draw3DAPI* _drafter;
  unsigned int _idx;
public:
  InterfaceDrawing(drawNS::Draw3DAPI* drafter, unsigned int idx) : _drafter(drafter), _idx(idx) {};
  virtual void draw() = 0;
  virtual void clearShape() = 0;
  virtual int getIdx() = 0;
};

#endif
