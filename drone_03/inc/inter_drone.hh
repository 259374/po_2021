#ifndef INTER_DRONE_HH
#define INTER_DRONE_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "../Dr3D_gnuplot_api.hh"

class InterfaceDrone{
public:
  virtual void goForward(double distance) = 0;
  virtual void goUp(double distance) = 0;
  virtual void goSide(double distance) = 0;
  virtual void turn(RotationMatrix<3> rot) = 0;
  virtual void turnPropellers() = 0;
  virtual Vector<3> dronegetcenter() = 0;
};

#endif
