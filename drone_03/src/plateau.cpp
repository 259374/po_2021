#include <iostream>
#include <iterator>

#include "coor_system.hh"
#include "vector.hh"
#include "plateau.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

const Vector<3> & Plateau::operator [] (int indx) const{
  if (indx < 0 || indx > 17){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}

void Plateau::draw(){
  std::vector<std::vector<drawNS::Point3D>> vtmp;
  std::vector<drawNS::Point3D> top;
  std::vector<drawNS::Point3D> bottom;
  std::array<std::vector<drawNS::Point3D>, 18> sides;

  CoorSystem temp(_center, _orientation, _parent);

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }

  //Bottom
  for (int i=0; i<_number_of_corners; i++){
    bottom.push_back(convert(temp.calculate_point((*this)[i])));
  }
  bottom.push_back(convert(temp.calculate_point((*this)[0])));
  vtmp.push_back(bottom);

  //Top
  for (int i=0; i<_number_of_corners; i++){
    top.push_back(convert(temp.calculate_point((*this)[i+9])));
  }
  top.push_back(convert(temp.calculate_point((*this)[9])));
  vtmp.push_back(top);

  //Sides
  for (int i=0; i<_number_of_corners; i++){
    sides[i].push_back(convert(temp.calculate_point((*this)[i])));
    sides[i].push_back(convert(temp.calculate_point((*this)[i+9])));
    vtmp.push_back(sides[i]);
  }

  this->_idx = _drafter -> draw_polyhedron(vtmp, "blue");
}

void Plateau::clearShape(){
  _drafter->erase_shape(_idx);
}
