#include <iostream>
#include <iterator>

#include "coor_system.hh"
#include "hex_prism.hh"
#include "vector.hh"
#include "cuboid.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

const Vector<3> & HexagonalPrism::operator [] (int indx) const{
  if (indx < 0 || indx > 11){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}

void HexagonalPrism::draw(){
  std::vector<std::vector<drawNS::Point3D>> vtmp;
  std::vector<drawNS::Point3D> top;
  std::vector<drawNS::Point3D> bottom;
  std::vector<drawNS::Point3D> side1;
  std::vector<drawNS::Point3D> side2;
  std::vector<drawNS::Point3D> side3;
  std::vector<drawNS::Point3D> side4;
  std::vector<drawNS::Point3D> side5;
  std::vector<drawNS::Point3D> side6;

  CoorSystem temp(_center, _orientation, _parent);

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }

  //Top
  top.push_back(convert(temp.calculate_point((*this)[0])));
  top.push_back(convert(temp.calculate_point((*this)[1])));
  top.push_back(convert(temp.calculate_point((*this)[2])));
  top.push_back(convert(temp.calculate_point((*this)[3])));
  top.push_back(convert(temp.calculate_point((*this)[4])));
  top.push_back(convert(temp.calculate_point((*this)[5])));
  top.push_back(convert(temp.calculate_point((*this)[0])));

  vtmp.push_back(top);

  //Bottom
  bottom.push_back(convert(temp.calculate_point((*this)[6])));
  bottom.push_back(convert(temp.calculate_point((*this)[7])));
  bottom.push_back(convert(temp.calculate_point((*this)[8])));
  bottom.push_back(convert(temp.calculate_point((*this)[9])));
  bottom.push_back(convert(temp.calculate_point((*this)[10])));
  bottom.push_back(convert(temp.calculate_point((*this)[11])));
  bottom.push_back(convert(temp.calculate_point((*this)[6])));

  vtmp.push_back(bottom);

  //Sides
  side1.push_back(convert(temp.calculate_point((*this)[0])));
  side1.push_back(convert(temp.calculate_point((*this)[6])));
  vtmp.push_back(side1);

  side2.push_back(convert(temp.calculate_point((*this)[1])));
  side2.push_back(convert(temp.calculate_point((*this)[7])));
  vtmp.push_back(side2);

  side3.push_back(convert(temp.calculate_point((*this)[2])));
  side3.push_back(convert(temp.calculate_point((*this)[8])));
  vtmp.push_back(side3);

  side4.push_back(convert(temp.calculate_point((*this)[3])));
  side4.push_back(convert(temp.calculate_point((*this)[9])));
  vtmp.push_back(side4);

  side5.push_back(convert(temp.calculate_point((*this)[4])));
  side5.push_back(convert(temp.calculate_point((*this)[10])));
  vtmp.push_back(side5);

  side6.push_back(convert(temp.calculate_point((*this)[5])));
  side6.push_back(convert(temp.calculate_point((*this)[11])));
  vtmp.push_back(side6);

  this->_idx = _drafter -> draw_polyhedron(vtmp, "red");
}

void HexagonalPrism::clearShape(){
  _drafter->erase_shape(_idx);
}
