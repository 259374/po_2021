#include <iostream>
#include <iterator>

#include "inter_stage_object.hh"
#include "drone.hh"
#include "coor_system.hh"
#include "hex_prism.hh"
#include "vector.hh"
#include "cuboid.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

void Drone::draw(){
  body.draw();
  for (int i=0; i<4; i++){
    (propellers[i]).draw();
  }
}

void Drone::goForward(double distance){
  this->move(Vector<3>({distance, 0, 0}));
}
void Drone::goUp(double distance){
  this->move(Vector<3>({0, 0, distance}));
}
void Drone::goSide(double distance){
  this->move(Vector<3>({0, distance, 0}));
}

void Drone::turn(RotationMatrix<3> rot){
  this->rotate(rot);
}

void Drone::turnPropellers(){
  for (int i=0; i<4; i++){
    propellers[i].rotate(RotationMatrix<3>(15, Vector<3>({0,0,1})));
  }
}

void Drone::animate(double distanceForward, double distanceSide, double distanceUp, drawNS::Draw3DAPI *drafter){
  double velocity = 0.1;
  double forward_frames = abs(distanceForward)/velocity;
  double side_frames = abs(distanceSide)/velocity;
  double up_frames = abs(distanceUp)/velocity;
  double frwd_lean = 1, side_lean = 1, lean_frames;

  //Lean direction
  if (distanceForward < 0) frwd_lean = -1;
  if (distanceSide < 0) side_lean = -1;

  //this->turn(RotationMatrix<3>(frwd_lean * 15, Vector<3>({0,1,0}) ));
  lean_frames = forward_frames / 3;
  for (int i=0; i<forward_frames; i++){
    if (i < lean_frames){
      this->turn(RotationMatrix<3>(frwd_lean * 15 / lean_frames, Vector<3>({0,1,0}) ));
    }
    if (i >= forward_frames - lean_frames - 1){
      this->turn(RotationMatrix<3>((-1) * frwd_lean * 15 / lean_frames, Vector<3>({0,1,0}) ));
    }
    this->goForward(frwd_lean * velocity);
    this->turnPropellers();
    this->clearShape();
    this->draw();
    std::this_thread::sleep_for(std::chrono::milliseconds(40));
  }
  //this->turn(RotationMatrix<3>((-1) * frwd_lean * 15, Vector<3>({0,1,0}) ));

  lean_frames = side_frames / 3;
  //this->turn(RotationMatrix<3>(side_lean * 15, Vector<3>({1,0,0}) ));
  for (int i=0; i<side_frames; i++){
    if (i < lean_frames){
      this->turn(RotationMatrix<3>(side_lean * (-15) / lean_frames, Vector<3>({1,0,0}) ));
    }
    if (i >= side_frames - lean_frames - 1){
      this->turn(RotationMatrix<3>((-1) * side_lean * (-15) / lean_frames, Vector<3>({1,0,0}) ));
    }
    this->goSide(side_lean * velocity);
    this->turnPropellers();
    this->clearShape();
    this->draw();
    std::this_thread::sleep_for(std::chrono::milliseconds(40));
  }
  //this->turn(RotationMatrix<3>((-1) * side_lean * 15, Vector<3>({1,0,0}) ));

  for (int i=0; i<up_frames; i++){
    this->goUp(velocity);
    this->turnPropellers();
    this->clearShape();
    this->draw();
    std::this_thread::sleep_for(std::chrono::milliseconds(40));
  }
}

void Drone::clearShape(){
  (this->getBody()).clearShape();
  for (int i=0; i<4; i++){
    (this->getPropeller(i)).clearShape();
  }
}
