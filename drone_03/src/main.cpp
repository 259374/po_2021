#include <iostream>

#include "stage.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "plateau_cuboid.hh"
#include "hill.hh"
#include "plateau.hh"
#include "vector.hh"
#include "plane.hh"
#include "coor_system.hh"
#include "cuboid.hh"
#include "hex_prism.hh"
#include "../Dr3D_gnuplot_api.hh"

using std::vector;
using drawNS::Point3D;
using drawNS::APIGnuPlot3D;
using std::cout;
using std::cin;
using std::cerr;
using std::endl;

void wait4key() {
  do {
    cout << "\nPress a key to continue..." << endl;
  } while(cin.get() != '\n');
}

void info(Drone &test){
  cout << endl << "Number of existing vectors: " << (test.getcenter()).get_exist_number() << endl;
  cout << "Number of created vectors: " << (test.getcenter()).get_created_number() << endl;
  cout << endl << "Drone center: " << test.getcenter() << endl;
  cout << "Drone orientation: " << endl << test.getorient() << endl;
}

int main(){
  Vector<3> axes_center({0,0,0});
  Vector<3> z_axis({0,0,1}), y_axis({0,1,0}), x_axis({1,0,0});
  RotationMatrix<3> identity_matrix(0, z_axis);
  std::string opt = "";
  unsigned int index;

  Vector<3> mv_vec = axes_center;
  Vector<3> rot_vec;
  RotationMatrix<3> rot = identity_matrix;
  double angle_degrees;
  double multiplier;
  char axis;

  drawNS::Draw3DAPI* drafter = new APIGnuPlot3D(-20,20,-20,20,-10,10,0);

  Stage stage(drafter);

  stage.drawEverything();
  while(opt[0] != 'k'){
    cout << "Wybierz opcje (m - menu) : ";
    cin >> opt;
    switch(opt[0]){
      case 'm':{
        cout << "\ne - dodaj element krajobrazu" << endl;
        cout << "y - usun element sceny" << endl;
        cout << "d - dodaj drona" << endl;
        cout << "u - usun drona" << endl;
        cout << "c - wybierz drona" << endl;
        cout << "p - przenies drona" << endl;
        cout << "m - wyswietl menu " << endl;
        cout << "w - wyswietl wszystkie obiekty sceny" << endl;
        cout << "k - koniec programu" << endl;
      }
      break;
      case 'e':{
        stage.addStageObject();
        stage.drawEverything();
      }
      break;
      case 'y':{
        cout << "Ktory obiekt usunac?" << endl;
        stage.showAllStageObjects();
        cout << "Twoj wybor: ";
        cin >> index;
        stage.deleteStageObject(index);
        stage.drawEverything();
      }
      break;
      case 'd':{
        stage.addDrone();
        stage.drawEverything();
      }
      break;
      case 'p':{
        cout << endl << "Podaj kat obrotu (w stopniach) : ";
        cin >> angle_degrees;
        cout << "Podaj ilosc powtorzen operacji : ";
        cin >> multiplier;
        angle_degrees *= multiplier;
        cout << "Podaj os obrotu (x, y ,z): ";
        cin >> axis;
        switch(axis){
          case 'x': rot_vec = x_axis;
          break;
          case 'y': rot_vec = y_axis;
          break;
          case 'z': rot_vec = z_axis;
          break;
          default: cout << "Brak podanej osi" << endl;
          break;
        }
        rot = RotationMatrix<3>(angle_degrees, rot_vec);
        cout << endl << "Podaj wspolrzedne wektora przesuniecia : ";
        cin >> mv_vec;
        stage.animate(mv_vec[0], mv_vec[1], mv_vec[2], rot);
      }
      break;
      case 'c':{
        cout << "Ktorego drona wybrac?" << endl;
        stage.showAllDrones();
        cout << "Twoj wybor: ";
        cin >> index;
        stage.selectDrone(index);
        stage.drawEverything();
      }
      break;
      case 'u':{
        cout << "Ktorego drona usunac?" << endl;
        stage.showAllDrones();
        cout << "Twoj wybor: ";
        cin >> index;
        stage.deleteDrone(index);
        stage.drawEverything();
      }
      break;
      case 'w':{
        stage.showAllStageObjects();
        stage.drawEverything();
      }
      break;
      case 'k': break;
      default:
        cout << "Wybierz jedna z opcji z menu" << endl;
      break;
    }
  wait4key();
  }
  delete drafter;
}
