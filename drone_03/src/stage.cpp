#include <iostream>
#include <array>
#include <random>
#include <cmath>
#include <algorithm>
#include <chrono>

#include "hill.hh"
#include "plateau.hh"
#include "plateau_cuboid.hh"
#include "stage.hh"
#include "inter_drone.hh"
#include "inter_drawing.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "../Dr3D_gnuplot_api.hh"

void Stage::animate(double distanceForward, double distanceSide, double distanceUp, RotationMatrix<3> rot){
  double velocity = 0.1;
  double forward_frames = abs(distanceForward)/velocity;
  double side_frames = abs(distanceSide)/velocity;
  double up_frames = abs(distanceUp)/velocity;
  double frwd_lean = 1, side_lean = 1, lean_frames;

  if(drone_list.size() == 0){
    std::cout << "Istnieje " << drone_list.size() << " dronow." <<std::endl;
  }
  else{
    std::shared_ptr<InterfaceDrawing> drawable_drone = std::dynamic_pointer_cast<InterfaceDrawing>(selected_drone);

    selected_drone->turn(rot);

    //Lean direction
    if (distanceForward < 0) frwd_lean = -1;
    if (distanceSide < 0) side_lean = -1;

    //selected_drone->turn(RotationMatrix<3>(frwd_lean * 15, Vector<3>({0,1,0}) ));
    lean_frames = forward_frames / 3;
    for (int i=0; i<forward_frames; i++){
      if (i < lean_frames){
        selected_drone->turn(RotationMatrix<3>(frwd_lean * 15 / lean_frames, Vector<3>({0,1,0}) ));
      }
      if (i >= forward_frames - lean_frames - 1){
        selected_drone->turn(RotationMatrix<3>((-1) * frwd_lean * 15 / lean_frames, Vector<3>({0,1,0}) ));
      }
      selected_drone->goForward(frwd_lean * velocity);
      selected_drone->turnPropellers();
      drawable_drone->clearShape();
      drawable_drone->draw();
      std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }
    //selected_drone->turn(RotationMatrix<3>((-1) * frwd_lean * 15, Vector<3>({0,1,0}) ));

    lean_frames = side_frames / 3;
    //selected_drone->turn(RotationMatrix<3>(side_lean * 15, Vector<3>({1,0,0}) ));
    for (int i=0; i<side_frames; i++){
      if (i < lean_frames){
        selected_drone->turn(RotationMatrix<3>(side_lean * (-15) / lean_frames, Vector<3>({1,0,0}) ));
      }
      if (i >= side_frames - lean_frames - 1){
        selected_drone->turn(RotationMatrix<3>((-1) * side_lean * (-15) / lean_frames, Vector<3>({1,0,0}) ));
      }
      selected_drone->goSide(side_lean * velocity);
      selected_drone->turnPropellers();
      drawable_drone->clearShape();
      drawable_drone->draw();
      std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }
    //selected_drone->turn(RotationMatrix<3>((-1) * side_lean * 15, Vector<3>({1,0,0}) ));

    for (int i=0; i<up_frames; i++){
      selected_drone->goUp(velocity);
      selected_drone->turnPropellers();
      drawable_drone->clearShape();
      drawable_drone->draw();
      std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }
  }
}

void Stage::drawEverything(){
  for (int i=0; i< (int)drawable_list.size(); ++i){
    drawable_list[i]->draw();
    std::cout << "Obiekt " << i << ". zostal narysowany" << std::endl;
  }
}


void Stage::addStageObject(){
  std::string opt = "";
  double x_size, y_size, z_size;
  double min_size, max_size;
  Vector<3> placement;
  RotationMatrix<3> identity_matrix(0, Vector<3>({0, 0, 1}));

  std::cout << "Jaki element krajobrazu dodac?" << std::endl << std::endl;
  std::cout << "p - plaskowyz prostopadloscienny" << std::endl;
  std::cout << "r - plaskowyz o losowym rozkladzie wierzcholkow" << std::endl;
  std::cout << "w - wzgorze o losowym rozkladzie wierzcholkow" << std::endl << std::endl;

  std::cout << "Wybierz opcje: ";
  std::cin >> opt;
  std::cout << std::endl;

  switch (opt[0]){
    case 'p':{
      std::cout << "Podaj wymiary plaskowyzu (x, y, z): ";
      std::cin >> x_size;
      std::cin >> y_size;
      std::cin >> z_size;
      std::cout << "Podaj umiejscowienie (x, y, z): ";
      std::cin >> placement;
      object_list.push_back(std::shared_ptr<InterfaceStageObject>(new PlateauCuboid(placement, identity_matrix, nullptr, x_size, y_size, z_size, _drafter)));
      drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
    }
    break;
    case 'r':{
      std::cout << "Podaj rozmiary plaskowyzu (min_promien_podstawy, max_promien_podstawy): ";
      std::cin >> min_size;
      std::cin >> max_size;
      std::cout << "Podaj wysokosc: ";
      std::cin >> z_size;
      std::cout << "Podaj umiejscowienie (x, y, z): ";
      std::cin >> placement;
      object_list.push_back(std::shared_ptr<InterfaceStageObject>(new Plateau(placement, identity_matrix, nullptr, z_size, min_size, max_size, _drafter)));
      drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
    }
    break;
    case 'w':{
      std::cout << "Podaj rozmiary wzgorza (min_promien_podstawy, max_promien_podstawy): ";
      std::cin >> min_size;
      std::cin >> max_size;
      std::cout << "Podaj wysokosc: ";
      std::cin >> z_size;
      std::cout << "Podaj umiejscowienie (x, y, z): ";
      std::cin >> placement;
      object_list.push_back(std::shared_ptr<InterfaceStageObject>(new Hill(placement, identity_matrix, nullptr, z_size, min_size, max_size, _drafter)));
      drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
    }
    break;
    default: std::cout << "Brak podanej opcji" << std::endl;
    break;
  }
}

void Stage::deleteStageObject(unsigned int id){
  if (id > object_list.size()){
    std::cout << "Prosze wybrac obiekt z listy: " << std::endl;
    this->showAllStageObjects();
  }
  else if(object_list.size() == 0){
    std::cout << "Istnieje " << object_list.size() << " obiektow." <<std::endl;
  }
  else{
    std::shared_ptr<InterfaceStageObject> obj_ptr = object_list[id];
    std::shared_ptr<InterfaceDrawing> drawable_ptr = std::dynamic_pointer_cast<InterfaceDrawing>(obj_ptr);

    (drawable_ptr.get())->clearShape();

    object_list.erase(std::remove(object_list.begin(), object_list.end(), obj_ptr), object_list.end());
    drawable_list.erase(std::remove(drawable_list.begin(), drawable_list.end(), drawable_ptr), drawable_list.end());
  }
}

void Stage::showAllStageObjects(){
  for (int i=0; i< (int)object_list.size(); ++i){
    std::cout << "Obiekt " << i << ". ma srodek w: " << object_list[i]->getcenter() << std::endl;
  }
}
void Stage::addDrone(){
  Vector<3> placement;
  RotationMatrix<3> identity_matrix(0, Vector<3>({0, 0, 1}));

  std::cout << "Podaj umiejscowienie (x, y, z): ";
  std::cin >> placement;
  drone_list.push_back(std::shared_ptr<InterfaceDrone>(new Drone(placement, identity_matrix, nullptr, _drafter)));
  drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(drone_list.back()));

  this->selectDrone(drone_list.size()-1);
}
void Stage::deleteDrone(unsigned int id){
  if (id > drone_list.size()){
    std::cout << "Prosze wybrac drona z listy: " << std::endl;
    this->showAllDrones();
  }
  else if(drone_list.size() == 0){
    std::cout << "Istnieje " << drone_list.size() << " dronow." <<std::endl;
  }
  else{
    std::shared_ptr<InterfaceDrone> drone_ptr = drone_list[id];
    std::shared_ptr<InterfaceDrawing> drawable_ptr = std::dynamic_pointer_cast<InterfaceDrawing>(drone_ptr);

    (drawable_ptr.get())->clearShape();

    drawable_list.erase(std::remove(drawable_list.begin(), drawable_list.end(), drawable_ptr), drawable_list.end());
    drone_list.erase(std::remove(drone_list.begin(), drone_list.end(), drone_ptr), drone_list.end());
  }
}
void Stage::showAllDrones(){
  for (int i=0; i< (int)drone_list.size(); ++i){
    std::cout << "Dron " << i << ". ma srodek w: " << drone_list[i]->dronegetcenter() << std::endl;
  }
}

void Stage::selectDrone(unsigned int id){
  if (id > drone_list.size()){
    std::cout << "Prosze wybrac drona z listy: " << std::endl;
    this->showAllDrones();
  }
  else if(drone_list.size() == 0){
    std::cout << "Istnieje " << drone_list.size() << " dronow." <<std::endl;
  }
  else{
    this->setSelectedDrone(drone_list[id]);
  }
}
