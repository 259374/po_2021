#include <iostream>
#include <iterator>

#include "vector.hh"
#include "coor_system.hh"

CoorSystem CoorSystem::calculate_from_parent(const CoorSystem & parent){
  CoorSystem outcome;
  outcome._center = parent._orientation * this->_center + parent._center;
  outcome._orientation = this->_orientation * parent._orientation;
  outcome._parent = parent._parent;
  return outcome;
}

Vector<3> CoorSystem::calculate_point(Vector<3> local_point) const{
  return _orientation * local_point + _center;
}

drawNS::Point3D convert (Vector<3> vec){
  drawNS::Point3D outcome(vec[0],vec[1],vec[2]);
  return outcome;
}
