#ifndef DRONE_HH
#define DRONE_HH

#include <iostream>
#include <cmath>
#include <array>
#include <thread>
#include <chrono>

#include "vector.hh"
#include "coor_system.hh"
#include "cuboid.hh"
#include "hex_prism.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

class Drone : protected CoorSystem{
  Cuboid body;
  std::array<HexagonalPrism, 4> propellers;
public:
  Drone() = default;
  Drone(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent):
  CoorSystem(position, orientation, parent),
  body(Vector<3>({0,0,0}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 1, 2, 4),
  propellers({
    HexagonalPrism(Vector<3> ({-1,2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75),
    HexagonalPrism(Vector<3> ({-1,-2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75),
    HexagonalPrism(Vector<3> ({1,2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75),
    HexagonalPrism(Vector<3> ({1,-2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75)
  }) {};
  void draw(drawNS::Draw3DAPI *drawing);

  void goForward(double distance);
  void goUp(double distance);
  void goSide(double distance);
  void turn(RotationMatrix<3> rot);
  void turnPropellers();
  void animate(double distanceForward, double distanceSide, double distanceUp, drawNS::Draw3DAPI *drafter);

  void clearShape(drawNS::Draw3DAPI *drafter) const;

  //Get
  Vector<3> getcenter() const {return getPosition();};
  RotationMatrix<3> getorient() const {return getOrient();};

  Cuboid getBody() const {return body;};
  HexagonalPrism getPropeller(int which_one) const {return propellers[which_one];};
};

#endif
