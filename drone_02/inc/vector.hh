#ifndef VECTOR_HH
#define VECTOR_HH

#include <iostream>
#include <array>

template <int SIZE>
class Vector{
private:
  std::array<double, SIZE> coor;
  static int exist_number;
  static int created_number;
public:
  //Constructors
  Vector() {exist_number++; created_number++;};
  Vector(std::array<double, SIZE> arg){
    for (int i=0; i<SIZE; i++) coor[i] = arg[i];
    exist_number++; created_number++;
  };
  Vector(const Vector & W) {
    for (int i=0; i<SIZE; i++) this->coor[i] = W.coor[i];
    exist_number++; created_number++;
  };

  //Arithmetics
  Vector<SIZE> operator + (const Vector<SIZE> & arg2) const;
  Vector<SIZE> & operator += (const Vector<SIZE> & arg2);
  Vector<SIZE> operator - (const Vector<SIZE> & arg2) const;
  Vector<SIZE> operator * (double arg2) const;
  double operator * (const Vector<SIZE> & arg2) const;
  double length() const;

  //Get/Set
  static int get_exist_number() {return exist_number;};
  static int get_created_number() {return created_number;};
  double & operator[] (int indx);
  const double & operator [] (int indx) const;

  //Destructor
  ~Vector() {exist_number--;};
};

template <int SIZE>
int Vector<SIZE>::exist_number = 0;

template <int SIZE>
int Vector<SIZE>::created_number = 0;

template <int SIZE>
std::ostream & operator << (std::ostream & strm, const Vector<SIZE> & vec);

template <int SIZE>
std::istream & operator >> (std::istream & strm, Vector<SIZE> & vec);

template <int SIZE>
class RotationMatrix{
private:
  std::array<Vector<SIZE>, SIZE> rows;
public:
  //Constructors
  RotationMatrix() = default;
  RotationMatrix(double _angle_deg, Vector<SIZE> _axis);

  //Arithmetics
  Vector<SIZE> operator * (const Vector<SIZE> & arg2) const;
  RotationMatrix<SIZE> operator * (const RotationMatrix<SIZE> & arg2) const;
  RotationMatrix<SIZE> transpose() const;

  //Get/Set
  const double operator () (int ind_row, int ind_col) const;
  const Vector<SIZE> operator [] (int indx) const;
};


template <int SIZE>
RotationMatrix<SIZE>::RotationMatrix(double angle_deg, Vector<SIZE> axis){
  std::cerr << "Brak hiperobrotow" << std::endl;
}

template <int SIZE>
std::ostream & operator << (std::ostream & strm, const  RotationMatrix<SIZE> & rt_mtrx);

#endif
