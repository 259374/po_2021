#include <iostream>
#include <iterator>


#include "vector.hh"
#include "cuboid.hh"
#include "../Dr3D_gnuplot_api.hh"

#define EPSILON 10e-24

void cuboid::rotate(RotationMatrix<3> rot){
  this->corners[0] = rot * this->corners[0];
  this->corners[1] = rot * this->corners[1];
  this->corners[2] = rot * this->corners[2];
  this->corners[3] = rot * this->corners[3];

  this->corners[4] = rot * this->corners[4];
  this->corners[5] = rot * this->corners[5];
  this->corners[6] = rot * this->corners[6];
  this->corners[7] = rot * this->corners[7];
}

void cuboid::move(Vector<3> mv_vec){
  this->corners[0] = this->corners[0] + mv_vec;
  this->corners[1] = this->corners[1] + mv_vec;
  this->corners[2] = this->corners[2] + mv_vec;
  this->corners[3] = this->corners[3] + mv_vec;

  this->corners[4] = this->corners[4] + mv_vec;
  this->corners[5] = this->corners[5] + mv_vec;
  this->corners[6] = this->corners[6] + mv_vec;
  this->corners[7] = this->corners[7] + mv_vec;
}

bool cuboid::isCuboid(){
  if (((this->corners[0] - this->corners[1]).length() - (this->corners[2] - this->corners[3]).length() < EPSILON)
    && ((this->corners[1] - this->corners[2]).length() - (this->corners[0] - this->corners[3]).length() < EPSILON)
    && ((this->corners[0] - this->corners[4]).length() - (this->corners[2] - this->corners[6]).length() < EPSILON)){
      if (((this->corners[0] - this->corners[1]) * (this->corners[1] - this->corners[2]) < EPSILON)
       && ((this->corners[0] - this->corners[1]) * (this->corners[0] - this->corners[4]) < EPSILON)){
          return true;
        }
      }
  return false;
}

drawNS::Point3D convert (Vector<3> vec){
  drawNS::Point3D outcome(vec[0],vec[1],vec[2]);
  return outcome;
}

const Vector<3> & cuboid::operator [] (int indx) const{
  if (indx < 0 || indx > 7){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}


void cuboid::draw(drawNS::Draw3DAPI *drafter){
  std::vector<drawNS::Point3D> vtmp;
  std::vector<std::vector<drawNS::Point3D>> vtmpc;

  //Top
  vtmp.push_back(convert((*this)[0]));
  vtmp.push_back(convert((*this)[1]));
  vtmp.push_back(convert((*this)[2]));
  vtmp.push_back(convert((*this)[3]));
  vtmp.push_back(convert((*this)[0]));

  vtmpc.push_back(vtmp);
  while (!vtmp.empty()) {vtmp.pop_back();}

  //Bottom
  vtmp.push_back(convert((*this)[4]));
  vtmp.push_back(convert((*this)[5]));
  vtmp.push_back(convert((*this)[6]));
  vtmp.push_back(convert((*this)[7]));
  vtmp.push_back(convert((*this)[4]));

  vtmpc.push_back(vtmp);
  while (!vtmp.empty()) {vtmp.pop_back();}

  //Sides
  vtmp.push_back(convert((*this)[0]));
  vtmp.push_back(convert((*this)[4]));
  vtmpc.push_back(vtmp);
  while (!vtmp.empty()) {vtmp.pop_back();}

  vtmp.push_back(convert((*this)[1]));
  vtmp.push_back(convert((*this)[5]));
  vtmpc.push_back(vtmp);
  while (!vtmp.empty()) {vtmp.pop_back();}

  vtmp.push_back(convert((*this)[2]));
  vtmp.push_back(convert((*this)[6]));
  vtmpc.push_back(vtmp);
  while (!vtmp.empty()) {vtmp.pop_back();}

  vtmp.push_back(convert((*this)[3]));
  vtmp.push_back(convert((*this)[7]));
  vtmpc.push_back(vtmp);

  this->idx = drafter -> draw_polyhedron(vtmpc, "blue");
}
