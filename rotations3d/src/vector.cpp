#include <iostream>
#include <cmath>
#include <array>

#include "vector.hh"

#define M_PI 3.14159265358979323846

using std::cerr;
using std::endl;


//Adding two vectors
template <int SIZE>
Vector<SIZE> Vector<SIZE>::operator + (const Vector<SIZE> & arg2) const{
  Vector outcome;
  for (int i=0; i<SIZE; i++){
    outcome.coor[i] = (*this)[i] + arg2.coor[i];
  }
  return outcome;
}


//Adding to the first vector
template <int SIZE>
Vector<SIZE> & Vector<SIZE>::operator += (const Vector<SIZE> & arg2){
  for (int i=0; i<SIZE; i++){
    (*this)[i] = (*this)[i] + arg2.coor[i];
  }
  return *this;
}


//Subtracting second vector from the first one
template <int SIZE>
Vector<SIZE> Vector<SIZE>::operator - (const Vector<SIZE> & arg2) const{
  Vector outcome;
  for (int i=0; i<SIZE; i++){
    outcome.coor[i] = (*this)[i] - arg2.coor[i];
  }
  return outcome;
}


//Multiplying vector by number
template <int SIZE>
Vector<SIZE> Vector<SIZE>::operator * (double arg2) const{
  Vector outcome;
  for (int i=0; i<SIZE; i++){
    outcome.coor[i] = (*this)[i] * arg2;
  }
  return outcome;
}


//Multiplying vectors
template <int SIZE>
double Vector<SIZE>::operator * (const Vector<SIZE> & arg2) const{
  double outcome;
  for (int i=0; i<SIZE; i++){
    outcome += (*this)[i]*arg2.coor[i];
  }
  return outcome;
}


//Getting the length of the vector
template <int SIZE>
double Vector<SIZE>::length() const{
  double outcome;
  for (int i=0; i<SIZE; i++){
    outcome += (*this)[i]*(*this)[i];
  }
  return sqrt(outcome);
}


//Getting the coordinate of the vector
template <int SIZE>
double & Vector<SIZE>::operator[] (int indx){
  if (indx < 0 || indx > SIZE){
    cerr << "Poza pamiecia" << endl;
    exit(0);
  }
  return coor[indx];
}
template <int SIZE>
const double & Vector<SIZE>::operator [] (int indx) const{
  return coor[indx];
}


//Displaying the coordinates of the vector
template <int SIZE>
std::ostream & operator << (std::ostream & strm, const Vector<SIZE> & vec){
  for (int i=0; i<SIZE; i++){
    strm << vec[i] << " ";
  }
  return strm;
}

//Filling the vector with given coordinates
template <int SIZE>
std::istream & operator >> (std::istream & strm, Vector<SIZE> & vec){
  double numtmp;
  for (int i=0; i<SIZE; i++){
    strm >> numtmp;
    vec[i] = numtmp;
  }
  return strm;
}

//Transposition of matrix
template <int SIZE>
RotationMatrix<SIZE> RotationMatrix<SIZE>::transpose(){
  RotationMatrix<SIZE> outcome;
  int i, j;
  for (i=0; i<SIZE; i++){
    for (j=0; j<SIZE; j++){
      outcome.rows[i][j] = rows[j][i];
    }
  }
  return outcome;
}

//Multiplying rotation matrix by vector getting new rotated vector
template <int SIZE>
Vector<SIZE> RotationMatrix<SIZE>::operator * (const Vector<SIZE> & arg2) const{
  Vector<SIZE> outcome;
  double sum;
  int i, j;
  for (i=0; i<SIZE; i++){
    sum = 0;
    for (j=0; j<SIZE; j++){
      sum += rows[i][j] * arg2[j];
    }
    outcome[i] = sum;
  }
  return outcome;
}

//Multiplying two rotation matricies
template <int SIZE>
RotationMatrix<SIZE> RotationMatrix<SIZE>::operator * (const RotationMatrix<SIZE> & arg2) const{
  RotationMatrix<SIZE> outcome;
  double sum;
  int i, j;
  for (i=0; i<SIZE; i++){
    sum = 0;
    for (j=0; j<SIZE; j++){
      sum += rows[i][j] * arg2(j, i);
    }
    outcome.rows[i][j] = sum;
  }
  return outcome;
}

//Displaying values from the matrix
template <int SIZE>
const double RotationMatrix<SIZE>::operator() (int ind_row, int ind_col) const{
  return rows[ind_row][ind_col];
}


//Displaying whole rotation matrix
template <int SIZE>
std::ostream & operator << (std::ostream & strm, const  RotationMatrix<SIZE> & rt_mtrx){
  for (int i=0; i<SIZE; i++){
    for (int j=0; j<SIZE; j++){
      strm << rt_mtrx(i, j) << " ";
    }
    strm << endl;
  }
  return strm;
}

//3d rotation matrix
template<>
RotationMatrix<3>::RotationMatrix(double angle_deg, Vector<3> axis){
  //src: https://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm
  double theta, c, s, t;
  double x, y, z;

  theta = M_PI * (angle_deg / 180);
  c = cos(theta);
  s = sin(theta);
  t = 1 - c;

  x = axis[0];
  y = axis[1];
  z = axis[2];

  rows[0][0] = t*x*x+c;   rows[0][1] = t*x*y-z*s; rows[0][2] = t*x*z+y*s;
  rows[1][0] = t*x*y+z*s; rows[1][1] = t*y*y+c;   rows[1][2] = t*y*z-x*s;
  rows[2][0] = t*x*z-y*s; rows[2][1] = t*y*z+x*s; rows[2][2] = t*z*z+c;
}

//2d rotation matrix
template<>
RotationMatrix<2>::RotationMatrix(double angle_deg, Vector<2> axis){
  //src: https://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm
  double theta, c, s;

  theta = M_PI * (angle_deg / 180);
  c = cos(theta);
  s = sin(theta);

  rows[0][0] = c;        rows[0][1] = (-1) * s;
  rows[1][0] = s;        rows[1][1] = c;
}

template class Vector<2>;
template class Vector<3>;
template class Vector<6>;

template std::istream & operator >> (std::istream & strm, Vector<2> & vec);
template std::istream & operator >> (std::istream & strm, Vector<3> & vec);

template std::ostream & operator << (std::ostream & strm, const Vector<2> & vec);
template std::ostream & operator << (std::ostream & strm, const Vector<3> & vec);

template std::ostream & operator << (std::ostream & strm, const  RotationMatrix<2> & rt_mtrx);
template std::ostream & operator << (std::ostream & strm, const  RotationMatrix<3> & rt_mtrx);

template class RotationMatrix<2>;
template class RotationMatrix<3>;
