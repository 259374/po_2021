#include <iostream>


#include "vector.hh"
#include "cuboid.hh"
//#include "rectangle.hh"
//#include "../Dr2D_gnuplot_api.hh"
#include "../Dr3D_gnuplot_api.hh"

using std::vector;
using drawNS::Point3D;
using drawNS::APIGnuPlot3D;
using std::cout;
using std::cin;
using std::cerr;
using std::endl;

void wait4key() {
  do {
    cout << "\nPress a key to continue..." << endl;
  } while(cin.get() != '\n');
}

void cuboidSideInfo(cuboid cub){
  if(!cub.isCuboid()){
    cout << "\nBoki nie sa sobie rowne :(\n" << endl;
  }
  else{
    cout << "\nBoki sa sobie rowne :)\n" << endl;
  }
}


int main(){
  Vector<3> a({2.0,3.0,7.0}), b({2.0,8.0,7.0}), c({6.0, 8.0, 7.0}), d({6.0,3.0,7.0}),
            e({2.0,3.0,0.0}), f({2.0,8.0,0.0}), g({6.0, 8.0, 0.0}), h({6.0,3.0,0.0});
  /*
  Cuboid is going to look like this:
      a----------------d
    b-----------------c|
    |                  |
    |                  |
    |                  |
    | e----------------h
    f-----------------g

  */
  Vector<3> mv_vec;
  Vector<3> rot_vec;
  Vector<3> z_axis({0,0,1}), y_axis({0,1,0}), x_axis({1,0,0});
  cuboid cub({a,b,c,d,e,f,g,h});
  RotationMatrix<3> rot(60.0, z_axis);
  double angle_degrees;
  double multiplier;
  char axis;
  std::string opt = "";

  drawNS::Draw3DAPI * drafter = new APIGnuPlot3D(-10,10,-10,10,-10,10,1000);
  cub.draw(drafter);

  cout.precision(20);

  cuboidSideInfo(cub);

  while(opt[0] != 'k'){
  cout << "Wybierz opcje (m - menu) : ";
  cin >> opt;
  switch(opt[0]){
    case 'w':{
      cout << endl << "Wspolrzedne wierzcholkow: " << endl;
      cout << "Lewy gorny gornej sciany: " << cub[0] << endl;
      cout << "Lewy dolny gornej sciany: " << cub[1] << endl;
      cout << "Prawy dolny gornej sciany: " << cub[2] << endl;
      cout << "Prawy gorny gornej sciany: " << cub[3] << endl;

      cout << "Lewy gorny dolnej sciany: " << cub[0] << endl;
      cout << "Lewy dolny dolnej sciany: " << cub[1] << endl;
      cout << "Prawy dolny dolnej sciany: " << cub[2] << endl;
      cout << "Prawy gorny dolnej sciany: " << cub[3] << endl;
    }
    break;
    case 'm':{
      cout << "\no - obrot o zadany kat" << endl;
      cout << "p - przesuniecie o zadany wektor" << endl;
      cout << "m - wyswietl menu " << endl;
      cout << "w - wyswietl wspolrzedne" << endl;
      cout << "r - wyswietl macierz rotacji" << endl;
      cout << "k - koniec programu" << endl;
    }
    break;
    case 'p':{
      cout << endl << "Podaj wspolrzedne wektora przesuniecia : ";
      cin >> mv_vec;
      drafter->erase_shape(cub.getIdx());
      cub.move(mv_vec);
      cub.draw(drafter);
      cuboidSideInfo(cub);
    }
    break;
    case 'o':{
      drafter->erase_shape(cub.getIdx());
      cout << endl << "Podaj kat obrotu (w stopniach) : ";
      cin >> angle_degrees;
      cout << "Podaj ilosc powtorzen operacji : ";
      cin >> multiplier;
      angle_degrees *= multiplier;
      cout << "Podaj os obrotu (x, y ,z): ";
      cin >> axis;
      switch(axis){
        case 'x': rot_vec = x_axis;
        break;
        case 'y': rot_vec = y_axis;
        break;
        case 'z': rot_vec = z_axis;
        break;
        default: cout << "Brak podanej osi" << endl;
        break;
      }
      rot = RotationMatrix<3>(angle_degrees, rot_vec);
      cub.rotate(rot);
      cub.draw(drafter);
      cuboidSideInfo(cub);
    }
    break;
    case 'r':{
      cout << endl << rot;
    }
    break;
    case 'k': break;
    default:
      cout << "Wybierz jedna z opcji z menu" << endl;
    break;

  }
  wait4key();
  }

  delete drafter;
}
