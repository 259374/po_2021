#ifndef RECTANGLE_HH
#define RECTANGLE_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "../Dr2D_gnuplot_api.hh"

class rectangle{
  std::array<Vector<2>, 4> corners;
  int idx; //for drawing
public:
  rectangle() = default;
  rectangle(Vector<2> _left_top, Vector<2> _left_bot,
            Vector<2> _right_top, Vector<2> _right_bot){
              corners[0] = _left_top; corners[1] = _left_bot;
              corners[2] = _right_top; corners[3] = _right_bot;
            };
  void rotate(RotationMatrix<2> rot);
  void move(Vector<2> mv_vec);
  bool isRectangle();
  void draw(drawNS::Draw3DAPI *drawing);
  int getIdx(){return idx;};

  const Vector<2> & operator [] (int indx) const;
};

drawNS::Point3D convert (Vector<2> vec);

#endif
