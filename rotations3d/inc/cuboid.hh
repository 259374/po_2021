#ifndef CUBOID_HH
#define CUBOID_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "../Dr3D_gnuplot_api.hh"

class cuboid{
  std::array<Vector<3>, 8> corners;
  int idx; //for drawing
public:
  cuboid() = default;
  cuboid(std::array<Vector<3>, 8> _corners){
    corners[0] = _corners[0]; corners[1] = _corners[1];
    corners[2] = _corners[2]; corners[3] = _corners[3];

    corners[4] = _corners[4]; corners[5] = _corners[5];
    corners[6] = _corners[6]; corners[7] = _corners[7];
  }
  void rotate(RotationMatrix<3> rot);
  void move(Vector<3> mv_vec);
  bool isCuboid();
  int getIdx(){return idx;};
  void draw(drawNS::Draw3DAPI *drawing);

  const Vector<3> & operator [] (int indx) const;
};

drawNS::Point3D convert (Vector<3> vec);

#endif
