#ifndef VECTOR_HH
#define VECTOR_HH

#include <iostream>
#include <array>

template <int SIZE>
class Vector{
private:
  std::array<double, SIZE> coor;
public:
  //Constructors
  Vector() = default;
  Vector(std::array<double, SIZE> arg) : coor(arg) {};

  //Arithmetics
  Vector<SIZE> operator + (const Vector<SIZE> & arg2) const;
  Vector<SIZE> & operator += (const Vector<SIZE> & arg2);
  Vector<SIZE> operator - (const Vector<SIZE> & arg2) const;
  Vector<SIZE> operator * (double arg2) const;
  double operator * (const Vector<SIZE> & arg2) const;
  double length() const;

  //Get/Set
  double & operator[] (int indx);
  const double & operator [] (int indx) const;
};

template <int SIZE>
std::ostream & operator << (std::ostream & strm, const Vector<SIZE> & vec);

template <int SIZE>
std::istream & operator >> (std::istream & strm, Vector<SIZE> & vec);

template <int SIZE>
class RotationMatrix{
private:
  std::array<Vector<SIZE>, SIZE> rows;
public:
  //Constructors
  RotationMatrix() = default;
  RotationMatrix(double _angle_deg, Vector<SIZE> _axis);

  //Arithmetics
  Vector<SIZE> operator * (const Vector<SIZE> & arg2) const;
  RotationMatrix<SIZE> operator * (const RotationMatrix<SIZE> & arg2) const;
  RotationMatrix<SIZE> transpose();

  //Get/Set
  const double operator () (int ind_row, int ind_col) const;
};


template <int SIZE>
RotationMatrix<SIZE>::RotationMatrix(double angle_deg, Vector<SIZE> axis){
  std::cerr << "Brak hiperobrotow" << std::endl;
}

template <int SIZE>
std::ostream & operator << (std::ostream & strm, const  RotationMatrix<SIZE> & rt_mtrx);


#endif
