#include <iostream>

#include "OpenGL_API.hh"
#include "stage.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "plateau_cuboid.hh"
#include "hill.hh"
#include "plateau.hh"
#include "vector.hh"
#include "plane.hh"
#include "coor_system.hh"
#include "cuboid.hh"
#include "hex_prism.hh"

const double axes_size = 20;

using std::vector;
using drawNS::Point3D;
using drawNS::APIopenGL3D;
using std::cout;
using std::cin;
using std::cerr;
using std::endl;


void wait4key() {
  do {
    cout << "\nPress a key to continue..." << endl;
  } while(cin.get() != '\n');
}

int main(int argc, char **argv){
  Vector<3> axes_center({0,0,0});
  Vector<3> z_axis({0,0,1}), y_axis({0,1,0}), x_axis({1,0,0});
  RotationMatrix<3> identity_matrix(0, z_axis);
  std::string opt = "";
  unsigned int index;
  double flight_length;
  double flight_angle_deg;
  RotationMatrix<3> rot = identity_matrix;

  auto * drafter = new APIopenGL3D(-40,40,-40,40,-40,40,0,&argc,argv);
  Stage stage(drafter);
  Plane plane(axes_center, identity_matrix, nullptr, 0, drafter);

  while(opt[0] != 'k'){
    plane.draw();
    stage.drawEverything();
    cout << "Wybierz opcje (m - menu) : ";
    cin >> opt;
    switch(opt[0]){
      case 'm':{
        cout << "\ne - dodaj element krajobrazu" << endl;
        cout << "y - usun element sceny" << endl;
        cout << "d - dodaj drona" << endl;
        cout << "u - usun drona" << endl;
        cout << "c - wybierz drona" << endl;
        cout << "p - przenies drona" << endl;
        cout << "m - wyswietl menu " << endl;
        cout << "w - wyswietl wszystkie obiekty sceny" << endl;
        cout << "j - wyswietl wszystkie obiekty z kolizja" << endl;
        cout << "k - koniec programu" << endl;
      }
      break;
      case 'e':{
        stage.addStageObject();
      }
      break;
      case 'y':{
        cout << "Ktory obiekt usunac?" << endl;
        stage.showAllStageObjects();
        cout << "Twoj wybor: ";
        cin >> index;
        stage.deleteStageObject(index);
      }
      break;
      case 'd':{
        stage.addDrone();
      }
      break;
      case 'p':{
        cout << endl << "Podaj kat (w stopniach) przelotu: ";
        cin >> flight_angle_deg;
        cout << "Podaj dlugosc przelotu: ";
        cin >> flight_length;
        stage.animate(flight_length, flight_angle_deg);
      }
      break;
      case 'c':{
        cout << "Ktorego drona wybrac?" << endl;
        stage.showAllDrones();
        cout << "Twoj wybor: ";
        cin >> index;
        stage.selectDrone(index);
      }
      break;
      case 'u':{
        cout << "Ktorego drona usunac?" << endl;
        stage.showAllDrones();
        cout << "Twoj wybor: ";
        cin >> index;
        stage.deleteDrone(index);
      }
      break;
      case 'w':{
        stage.showAllStageObjects();
      }
      break;
      case 'j':{
        stage.showAllCollisionObjects();
      }
      break;
      case 'k': break;
      default:
        cout << "Wybierz jedna z opcji z menu" << endl;
      break;
    }
    stage.drawEverything();
    wait4key();
  }
  delete drafter;
}
