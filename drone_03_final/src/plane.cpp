#include <iostream>
#include <iterator>

#include "coor_system.hh"
#include "vector.hh"
#include "plane.hh"

#define EPSILON 10e-24

const Vector<3> & Plane::operator [] (int indx) const{
  if (indx < 0 || indx > 3){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}

void Plane::draw(){
  if(_idx != -1){
    _drafter->erase_shape(_idx);
  }
  std::vector<std::vector<drawNS::Point3D>> vtmp;
  std::vector<drawNS::Point3D> row;
  for (int i=-40; i <= 40; i+=2) {
      for (int j=-40; j <= 40; j+=2) {
          row.push_back(convert(Vector<3>({(double)i,(double)j,_height})));
      }
      vtmp.push_back(row);
      row.clear();
  }
  this->_idx = _drafter -> draw_surface(vtmp, "orange");
}

void Plane::clearShape(){
  _drafter->erase_shape(_idx);
}
