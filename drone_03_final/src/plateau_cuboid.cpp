#include <iostream>
#include <iterator>

#include "inter_stage_object.hh"
#include "plateau_cuboid.hh"
#include "coor_system.hh"
#include "vector.hh"
#include "cuboid.hh"
#include "drone.hh"

#define EPSILON 10e-24

bool PlateauCuboid::isAbove(InterfaceDrone* drone){
  std::array<Vector<3>, 4> corner;
  std::array<Vector<3>, 4> middles;
  double max_axis1, min_axis1, max_axis2, min_axis2;
  Vector<3> drone_center = drone->dronegetcenter();
  double drone_radius = drone->dronegetradius();

  CoorSystem temp(this->getcenter(), this->getOrient(), this->getParent());

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }

  for(int i=0; i<4; i++){
    corner[i] = temp.calculate_point((*this)[i]);
  }

  for(int i=1; i<=4; i++){
    (middles[i-1])[0] = ((corner[i-1])[0] + (corner[i%4])[0])/2;
    (middles[i-1])[1] = ((corner[i-1])[1] + (corner[i%4])[1])/2;
  }

  /*
  for(int i=0; i<4; i++){
    std::cout << i+1 << ". srodek ma x: " << (middles[i])[0] << " y: " << (middles[i])[1] << std::endl;
  }
  for(int i=0; i<4; i++){
    std::cout << i+1 << ". x: " << (corner[i])[0] << " y: " << (corner[i])[1] << std::endl;
  }
  */
  if((middles[0])[0] > (middles[2])[0]){
    max_axis1 = (middles[0])[0] + drone_radius;
    min_axis1 = (middles[2])[0] - drone_radius;
  }
  else{
    min_axis1 = (middles[0])[0] - drone_radius;
    max_axis1 = (middles[2])[0] + drone_radius;
  }

  if((middles[1])[1] > (middles[3])[1]){
    max_axis2 = (middles[1])[1] + drone_radius;
    min_axis2 = (middles[3])[1] - drone_radius;
  }
  else{
    min_axis2 = (middles[1])[1] - drone_radius;
    max_axis2 = (middles[3])[1] + drone_radius;
  }

  if((drone_center[0] > min_axis1 && drone_center[0] < max_axis1)
   && drone_center[1] > min_axis2 && drone_center[1] < max_axis2){
     return true;
   }

  return false;
}

double PlateauCuboid::isLandingPossible(InterfaceDrone* drone){
  std::array<Vector<3>, 4> corner;
  std::array<Vector<3>, 4> middles;
  double max_axis1, min_axis1, max_axis2, min_axis2;
  Vector<3> drone_center = drone->dronegetcenter();

  CoorSystem temp(this->getcenter(), this->getOrient(), this->getParent());

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }

  for(int i=0; i<4; i++){
    corner[i] = temp.calculate_point((*this)[i]);
  }

  for(int i=1; i<=4; i++){
    (middles[i-1])[0] = ((corner[i-1])[0] + (corner[i%4])[0])/2;
    (middles[i-1])[1] = ((corner[i-1])[1] + (corner[i%4])[1])/2;
  }

  if((middles[0])[0] > (middles[2])[0]){
    max_axis1 = (middles[0])[0];
    min_axis1 = (middles[2])[0];
  }
  else{
    min_axis1 = (middles[0])[0];
    max_axis1 = (middles[2])[0];
  }

  if((middles[1])[1] > (middles[3])[1]){
    max_axis2 = (middles[1])[1];
    min_axis2 = (middles[3])[1];
  }
  else{
    min_axis2 = (middles[1])[1];
    max_axis2 = (middles[3])[1];
  }

  if((drone_center[0] > min_axis1 && drone_center[0] < max_axis1)
   && drone_center[1] > min_axis2 && drone_center[1] < max_axis2){
     return (drone_center[2]-this->getHeight()-0.5);
   }
  return -1;
}
