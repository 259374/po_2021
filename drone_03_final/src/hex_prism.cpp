#include <iostream>
#include <iterator>

#include "coor_system.hh"
#include "hex_prism.hh"
#include "vector.hh"
#include "cuboid.hh"

#define EPSILON 10e-24

const Vector<3> & HexagonalPrism::operator [] (int indx) const{
  if (indx < 0 || indx > 11){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}

void HexagonalPrism::draw(){
  if(_idx != -1){
    _drafter->erase_shape(_idx);
  }
  std::vector<std::vector<drawNS::Point3D>> vtmp;
  std::vector<drawNS::Point3D> top;
  std::vector<drawNS::Point3D> bottom;

  CoorSystem temp(_center, _orientation, _parent);

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }


  //Bottom
  for (int i=6; i<12; i++){
    bottom.push_back(convert(temp.calculate_point((*this)[i])));
  }
  vtmp.push_back(bottom);

  //Top
  for (int i=0; i<6; i++){
    top.push_back(convert(temp.calculate_point((*this)[i])));
  }
  vtmp.push_back(top);

  this->_idx = _drafter -> draw_polyhedron(vtmp, "red");
}

void HexagonalPrism::clearShape(){
  _drafter->erase_shape(_idx);
}
