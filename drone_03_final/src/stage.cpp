#include <iostream>
#include <array>
#include <random>
#include <cmath>
#include <algorithm>
#include <chrono>
#include <memory>

#include "hill.hh"
#include "plateau.hh"
#include "plateau_cuboid.hh"
#include "stage.hh"
#include "inter_drone.hh"
#include "inter_drawing.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"

#define M_PI 3.14159265358979323846

const double flight_height = 12;
const double correction_step = 3;

void Stage::drawCourse(double length, double angle){
  std::vector<drawNS::Point3D> course_points;
  RotationMatrix<3> initial_matrix((-1)*angle, Vector<3>({0,0,1}));
  Vector<3> starting_point, starting_top, finish_top, finish_point;

  CoorSystem temp(selected_drone->dronegetcenter(), selected_drone->dronegetorient(), selected_drone->dronegetparent());

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }

  starting_point = temp.getPosition();
  //std::cout << "Started point found at: " << starting_point << std::endl;
  course_points.push_back(convert(starting_point));

  if(starting_point[2] != flight_height+0.5){
    starting_top = Vector<3>({starting_point[0], starting_point[1], flight_height+0.5});
    course_points.push_back(convert(starting_top));
    //std::cout << "Rising point found at: " << starting_top << std::endl;
  }
  finish_top = temp.calculate_point(initial_matrix * Vector<3>({0, length, flight_height-starting_point[2]+0.5}));
  //std::cout << "Finish top found at: " << finish_top << std::endl;
  finish_point = temp.calculate_point(initial_matrix * Vector<3>({0, length, 0-starting_point[2]}));
  course_points.push_back(convert(finish_top));
  //std::cout << "Finish point found at: " << finish_point << std::endl;
  course_points.push_back(convert(finish_point));
  for (int i=0; i<(int)course_points.size()-1; i++){
    course_points_idx.push_back(_drafter->draw_line(course_points[i], course_points[i+1], "purple"));
  }
}

void Stage::correctCourse(double length){
  std::vector<drawNS::Point3D> course_points;
  Vector<3> starting_point, starting_top, finish_top, finish_point;

  CoorSystem temp(selected_drone->dronegetcenter(), selected_drone->dronegetorient(), selected_drone->dronegetparent());

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }

  _drafter->erase_shape(course_points_idx[course_points_idx.size()-1]);

  starting_point = temp.getPosition();
  //std::cout << "Corrected started point found at: " << starting_point << std::endl;
  course_points.push_back(convert(starting_point));

  finish_top = temp.calculate_point(Vector<3>({0,correction_step, 0}));
  //std::cout << "Corrected finish top found at: " << finish_top << std::endl;
  course_points.push_back(convert(finish_top));
  finish_point = temp.calculate_point(Vector<3>({0,correction_step, (-1)*flight_height}));
  course_points.push_back(convert(finish_point));
  //std::cout << "Corrected finish point found at: " << finish_point << std::endl;

  for (int i=0; i<(int)course_points.size()-1; i++){
    course_points_idx.push_back(_drafter->draw_line(course_points[i], course_points[i+1], "purple"));
  }
}

void Stage::clearCourse(){
  for (int i=0; i<(int)course_points_idx.size(); i++){
    _drafter->erase_shape(course_points_idx[i]);
  }

}

void Stage::animate(double length, double angle){
  double up_distance;
  double velocity = 0.3;
  double rising_velocity = 0.4;
  double rotation_velocity = 2;
  double landing_height = flight_height;
  int forward_frames, rotation_frames;
  int up_frames;
  double frwd_lean = 1, angle_lean =1;
  bool landed = false;
  bool flag = true;

  if(drone_list.size() == 0){
    std::cout << "Istnieje " << drone_list.size() << " dronow." <<std::endl;
  }
  else{
    std::shared_ptr<InterfaceDrawing> drawable_drone = std::dynamic_pointer_cast<InterfaceDrawing>(selected_drone);
    std::shared_ptr<InterfaceStageObject> object_drone = std::dynamic_pointer_cast<InterfaceStageObject>(selected_drone);

    up_distance = flight_height - (selected_drone->dronegetcenter())[2] + 0.5;
    up_frames = up_distance/rising_velocity;

    drawCourse(length, angle);
    //Calculating forward_frames
    forward_frames = length/velocity;

    //Calculating rotation_frames
    rotation_frames = abs(angle)/rotation_velocity;

    if (angle < 0) angle_lean = -1;

    for (int i=0; i<up_frames; i++){
      selected_drone->goUp(rising_velocity);
      selected_drone->turnPropellers();
      drawable_drone->draw();
      std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }

    for (int i=0; i<rotation_frames; i++){
      selected_drone->turn(RotationMatrix<3>((-1)*angle_lean*rotation_velocity, Vector<3>({0,0,1})));
      selected_drone->turnPropellers();
      drawable_drone->draw();
      std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }

    for (int i=0; i<forward_frames; i++){
      selected_drone->goForward(frwd_lean * velocity);
      selected_drone->turnPropellers();
      drawable_drone->draw();
      std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }

    while (!landed){
      for (int i=0; i<(int)collision_list.size(); i++){
        if(collision_list[i] != object_drone){
          if (collision_list[i]->isAbove(selected_drone.get())){
            if (collision_list[i]->isLandingPossible(selected_drone.get()) == -1){
              flag = false;
            }
            else{
              landing_height = collision_list[i]->isLandingPossible(selected_drone.get());
            }
          }
        }
      }
      if(!flag){
        std::cout << "Nie mozna wyladowac - zmiana przelotu" << std::endl;
        correctCourse(length);

        forward_frames = correction_step/velocity;

        for (int i=0; i<forward_frames; i++){
          selected_drone->goForward(frwd_lean * velocity);
          selected_drone->turnPropellers();
          drawable_drone->draw();
          std::this_thread::sleep_for(std::chrono::milliseconds(40));
        }
        flag = true;
      }
      else{
        landed = true;
      }
    }

    up_distance = landing_height;
    up_frames = up_distance/rising_velocity;

    for (int i=0; i<up_frames; i++){
      selected_drone->goUp((-1) * rising_velocity);
      selected_drone->turnPropellers();
      drawable_drone->draw();
      std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }
    clearCourse();
  }
}

void Stage::drawEverything() const{
  //std::cout << std::endl << "Liczba elementow do narysowania: " << drawable_list.size() << std::endl;
  for (int i=0; i< (int)drawable_list.size(); ++i){
    drawable_list[i]->draw();
    //std::cout << "Obiekt " << i << ". zostal narysowany" << std::endl;
  }
  std::cout << std::endl;
}

void Stage::addStageObject(){
  std::string opt = "";
  double x_size, y_size, z_size;
  double min_size=0, max_size=0;
  Vector<3> placement;
  RotationMatrix<3> identity_matrix(0, Vector<3>({0, 0, 1}));

  std::cout << std::endl;
  std::cout << "Jaki element krajobrazu dodac?" << std::endl << std::endl;
  std::cout << "p - plaskowyz prostopadloscienny" << std::endl;
  std::cout << "r - plaskowyz o losowym rozkladzie wierzcholkow" << std::endl;
  std::cout << "w - wzgorze o losowym rozkladzie wierzcholkow" << std::endl << std::endl;

  std::cout << "Wybierz opcje: ";
  std::cin >> opt;
  std::cout << std::endl;

  switch (opt[0]){
    case 'p':{
      std::cout << "Podaj wymiary plaskowyzu z, x, y: ";
      std::cin >> x_size;
      std::cin >> y_size;
      std::cin >> z_size;
      std::cout << "Podaj umiejscowienie x, y, z: ";
      std::cin >> placement;
      object_list.push_back(std::shared_ptr<InterfaceStageObject>(new PlateauCuboid(placement, identity_matrix, nullptr, x_size, y_size, z_size, _drafter)));
      drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
      collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(object_list.back()));
    }
    break;
    case 'r':{
      std::cout << "Podaj rozmiary plaskowyzu - min_promien_podstawy, max_promien_podstawy: ";
      std::cin >> min_size;
      std::cin >> max_size;
      if(min_size>=max_size){
        std::cout << "Zle wymiary" << std::endl;
        break;
      }
      std::cout << "Podaj wysokosc: ";
      std::cin >> z_size;
      std::cout << "Podaj umiejscowienie x, y, z: ";
      std::cin >> placement;
      object_list.push_back(std::shared_ptr<InterfaceStageObject>(new Plateau(placement, identity_matrix, nullptr, z_size, min_size, max_size, _drafter)));
      drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
      collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(object_list.back()));
    }
    break;
    case 'w':{
      std::cout << "Podaj rozmiary wzgorza - min_promien_podstawy, max_promien_podstawy: ";
      std::cin >> min_size;
      std::cin >> max_size;
      if(min_size>=max_size){
        std::cout << "Zle wymiary" << std::endl;
        break;
      }
      std::cout << "Podaj wysokosc: ";
      std::cin >> z_size;
      std::cout << "Podaj umiejscowienie x, y, z: ";
      std::cin >> placement;
      object_list.push_back(std::shared_ptr<InterfaceStageObject>(new Hill(placement, identity_matrix, nullptr, z_size, min_size, max_size, _drafter)));
      drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
      collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(object_list.back()));
    }
    break;
    default: std::cout << "Brak podanej opcji" << std::endl;
    break;
  }
}

void Stage::deleteStageObject(unsigned int id){
  if (id > object_list.size()-1){
    std::cout << "Prosze wybrac obiekt z listy: " << std::endl;
    this->showAllStageObjects();
  }
  else if(object_list.size() == 0){
    std::cout << "Istnieje " << object_list.size() << " obiektow." <<std::endl;
  }
  else{
    std::shared_ptr<InterfaceStageObject> obj_ptr = object_list[id];
    std::shared_ptr<InterfaceDrawing> drawable_ptr = std::dynamic_pointer_cast<InterfaceDrawing>(obj_ptr);
    std::shared_ptr<InterfaceStageObject> collision_ptr = std::dynamic_pointer_cast<InterfaceStageObject>(obj_ptr);

    drawable_ptr->clearShape();

    drawable_list.erase(std::remove(drawable_list.begin(), drawable_list.end(), drawable_ptr), drawable_list.end());
    collision_list.erase(std::remove(collision_list.begin(), collision_list.end(), collision_ptr), collision_list.end());
    object_list.erase(object_list.begin() + id);
  }
}

void Stage::showAllStageObjects() const{
  for (int i=0; i< (int)object_list.size(); ++i){
    std::cout << "Obiekt " << i << ". ma srodek w: " << object_list[i]->getcenter() << std::endl;
  }
}

void Stage::addDrone(){
  Vector<3> placement;
  RotationMatrix<3> identity_matrix(0, Vector<3>({0, 0, 1}));

  std::cout << "Podaj umiejscowienie x, y, z: ";
  std::cin >> placement;
  drone_list.push_back(std::shared_ptr<InterfaceDrone>(new Drone(placement, identity_matrix, nullptr, _drafter)));
  drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(drone_list.back()));
  collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(drone_list.back()));

  this->selectDrone(drone_list.size()-1);
  this->drawEverything();
}

void Stage::deleteDrone(unsigned int id){
  if (id > drone_list.size()-1){
    std::cout << "Prosze wybrac drona z listy: " << std::endl;
    this->showAllDrones();
  }
  else if(drone_list.size() == 0){
    std::cout << "Istnieje " << drone_list.size() << " dronow." <<std::endl;
  }
  else{
    std::shared_ptr<InterfaceDrone> drone_ptr = drone_list[id];
    std::shared_ptr<InterfaceDrawing> drawable_ptr = std::dynamic_pointer_cast<InterfaceDrawing>(drone_ptr);
    std::shared_ptr<InterfaceStageObject> collision_ptr = std::dynamic_pointer_cast<InterfaceStageObject>(drone_ptr);

    drawable_ptr->clearShape();

    drawable_list.erase(std::remove(drawable_list.begin(), drawable_list.end(), drawable_ptr), drawable_list.end());
    collision_list.erase(std::remove(collision_list.begin(), collision_list.end(), collision_ptr), collision_list.end());
    drone_list.erase(drone_list.begin() + id);

    if(drone_ptr == selected_drone){
      std::cout << "Usunieto aktywnego drona - ";
      if(drone_list.size() == 0){
        selected_drone = nullptr;
        std::cout << " ustawiono na nullptr" << std::endl;
      }
      else{
        selectDrone(drone_list.size()-1);
        std::cout << " ustawiono na przedostatniego drona z listy" << std::endl;
      }
    }
  }
}

void Stage::showAllDrones() const{
  for (int i=0; i< (int)drone_list.size(); ++i){
    std::cout << "Dron " << i << ". ma srodek w: " << drone_list[i]->dronegetcenter() << std::endl;
  }
}

void Stage::selectDrone(unsigned int id){
  if (id > drone_list.size()-1){
    std::cout << "Prosze wybrac drona z listy: " << std::endl;
    this->showAllDrones();
  }
  else if(drone_list.size() == 0){
    std::cout << "Istnieje " << drone_list.size() << " dronow." <<std::endl;
  }
  else{
    this->setSelectedDrone(id);
  }
}

void Stage::showAllCollisionObjects() const{
  for (int i=0; i< (int)collision_list.size(); ++i){
    std::cout << "Obiekt z kolizja o nr " << i << ". ma srodek w: " << collision_list[i]->getcenter() << std::endl;
  }
}
