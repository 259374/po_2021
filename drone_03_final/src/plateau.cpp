#include <iostream>
#include <iterator>

#include "coor_system.hh"
#include "vector.hh"
#include "plateau.hh"

#define EPSILON 10e-24

bool Plateau::isAbove(InterfaceDrone* drone){
  Vector<3> drone_center, object_center;
  double distance_between_centers;
  object_center =getcenter();
  drone_center = drone->dronegetcenter();
  /*
  std::cout << "Promien obiektu na ziemi: " << radius << std::endl;
  std::cout << "Promien drona w powietrzu: " << drone->dronegetradius() << std::endl;
  std::cout << "Wspolrzedne obiektu na ziemi: " << object_center << std::endl;
  std::cout << "Wspolrzedne drona w powietrzu: " << drone_center << std::endl;
  */

  distance_between_centers = sqrt((object_center[0]-drone_center[0])*(object_center[0]-drone_center[0])
                                   + (object_center[1]-drone_center[1])*(object_center[1]-drone_center[1]));
  //std::cout << "Dystans: " << distance_between_centers << std::endl;

  if (_max_radius + drone->dronegetradius() > distance_between_centers){
    return true;
  }
  return false;
}

double Plateau::isLandingPossible(InterfaceDrone* drone){
  Vector<3> drone_center, object_center;
  double distance_between_centers;
  object_center =getcenter();
  drone_center = drone->dronegetcenter();

  distance_between_centers = sqrt((object_center[0]-drone_center[0])*(object_center[0]-drone_center[0])
                                   + (object_center[1]-drone_center[1])*(object_center[1]-drone_center[1]));

  if (_min_radius + drone->dronegetradius() > distance_between_centers){
    return (drone_center[2] - _height - 0.5);
  }
  return -1;
}

const Vector<3> & Plateau::operator [] (int indx) const{
  if (indx < 0 || indx > 17){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}

void Plateau::draw(){
  if(_idx != -1){
    _drafter->erase_shape(_idx);
  }
  std::vector<std::vector<drawNS::Point3D>> vtmp;
  std::vector<drawNS::Point3D> top;
  std::vector<drawNS::Point3D> bottom;
  std::array<std::vector<drawNS::Point3D>, 18> sides;

  CoorSystem temp(_center, _orientation, _parent);

  while(temp.getParent() != nullptr){
    temp = temp.calculate_from_parent(*(temp.getParent()));
  }

  //Bottom
  for (int i=0; i<_number_of_corners; i++){
    bottom.push_back(convert(temp.calculate_point((*this)[i])));
  }
  bottom.push_back(convert(temp.calculate_point((*this)[0])));
  vtmp.push_back(bottom);

  //Top
  for (int i=0; i<_number_of_corners; i++){
    top.push_back(convert(temp.calculate_point((*this)[i+9])));
  }
  top.push_back(convert(temp.calculate_point((*this)[9])));
  vtmp.push_back(top);

  //Sides
  for (int i=0; i<_number_of_corners; i++){
    sides[i].push_back(convert(temp.calculate_point((*this)[i])));
    sides[i].push_back(convert(temp.calculate_point((*this)[i+9])));
    vtmp.push_back(sides[i]);
  }

  this->_idx = _drafter -> draw_polyhedron(vtmp, "blue");
}

void Plateau::clearShape(){
  _drafter->erase_shape(_idx);
}
