#include <iostream>
#include <iterator>

#include "inter_stage_object.hh"
#include "drone.hh"
#include "coor_system.hh"
#include "hex_prism.hh"
#include "vector.hh"
#include "cuboid.hh"

#define EPSILON 10e-24

bool Drone::isAbove(InterfaceDrone* drone){
  Vector<3> drone_center, object_center;
  double distance_between_centers;
  object_center = dronegetcenter();
  drone_center = drone->dronegetcenter();
  /*
  std::cout << "Promien drona na ziemi: " << radius << std::endl;
  std::cout << "Promien drona w powietrzu: " << drone->dronegetradius() << std::endl;
  std::cout << "Wspolrzedne drona na ziemi: " << object_center << std::endl;
  std::cout << "Wspolrzedne drona w powietrzu: " << drone_center << std::endl;
  */

  distance_between_centers = sqrt((object_center[0]-drone_center[0])*(object_center[0]-drone_center[0])
                                   + (object_center[1]-drone_center[1])*(object_center[1]-drone_center[1]));
  //std::cout << "Dystans: " << distance_between_centers << std::endl;

  if (radius + drone->dronegetradius() > distance_between_centers){
    return true;
  }
  return false;
}

void Drone::draw(){
  body.draw();
  for (int i=0; i<4; i++){
    (propellers[i]).draw();
  }
}

void Drone::goForward(double distance){
  this->move(_orientation * Vector<3>({0, distance, 0}));
}

void Drone::goUp(double distance){
  this->move(Vector<3>({0, 0, distance}));
}

void Drone::goSide(double distance){
  this->move(Vector<3>({distance, 0, 0}));
}

void Drone::turn(RotationMatrix<3> rot){
  this->rotate(rot);
}

void Drone::turnPropellers(){
  for (int i=0; i<4; i++){
    propellers[i].rotate(RotationMatrix<3>(15, Vector<3>({0,0,1})));
  }
}

void Drone::clearShape(){
  (this->getBody()).clearShape();
  for (int i=0; i<4; ++i){
    (this->getPropeller(i)).clearShape();
  }
}
