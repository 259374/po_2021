#ifndef STAGE_HH
#define STAGE_HH

#include <iostream>
#include <array>
#include <random>
#include <cmath>

#include "inter_drone.hh"
#include "inter_drawing.hh"
#include "plateau_cuboid.hh"
#include "hill.hh"
#include "plateau.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

/*!
 * \file
 * \brief File contains stage class
 */

/*!
* \brief Stage class used for defining features and behaviours of all objects on stage
*/
class Stage{
  /*!
  * \brief Vector containing shared pointers to drones on stage
  */
  std::vector<std::shared_ptr<InterfaceDrone>> drone_list;
  /*!
  * \brief Vector containing shared pointers to all landscape objects on stage
  */
  std::vector<std::shared_ptr<InterfaceStageObject>> object_list;
  /*!
  * \brief Vector containing shared pointers to all objects with collision on stage
  */
  std::vector<std::shared_ptr<InterfaceStageObject>> collision_list;
  /*!
  * \brief Vector containing shared pointers to all drawable objects on stage
  */
  std::vector<std::shared_ptr<InterfaceDrawing>> drawable_list;
  /*!
  * \brief Shared pointer to active drone
  */
  std::shared_ptr<InterfaceDrone> selected_drone;
  /*!
  * \brief Vector containing indexes of all course line shapes
  */
  std::vector<int> course_points_idx;
  /*!
  * \brief Pointer to drawing api
  */
  drawNS::APIopenGL3D* _drafter;
public:
  /*!
  * \brief Parametric constructor with parametres necessary for creating ready scene
  */
  Stage(drawNS::APIopenGL3D* drafter) : _drafter(drafter) {
    RotationMatrix<3> identity_matrix(0, Vector<3>({0, 0, 1}));
    //Adding one PlateauCuboid
    object_list.push_back(std::shared_ptr<InterfaceStageObject>(new PlateauCuboid(Vector<3>({9, 12, 0}), identity_matrix, nullptr, 2, 12, 18, _drafter)));
    drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
    collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(object_list.back()));
    //Adding one plateau
    object_list.push_back(std::shared_ptr<InterfaceStageObject>(new Plateau(Vector<3>({-9, -12, 0}), identity_matrix, nullptr, 3, 5, 12, _drafter)));
    drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
    collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(object_list.back()));
    //Adding one hill
    object_list.push_back(std::shared_ptr<InterfaceStageObject>(new Hill(Vector<3>({-9, 12, 0}), identity_matrix, nullptr, 8, 4, 10, _drafter)));
    drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(object_list.back()));
    collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(object_list.back()));
    //Adding first drone
    drone_list.push_back(std::shared_ptr<InterfaceDrone>(new Drone(Vector<3>({0, 0, 0}), identity_matrix, nullptr, _drafter)));
    drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(drone_list.back()));
    collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(drone_list.back()));
    //Adding second drone
    drone_list.push_back(std::shared_ptr<InterfaceDrone>(new Drone(Vector<3>({8, -8, 0}), identity_matrix, nullptr, _drafter)));
    drawable_list.push_back(std::dynamic_pointer_cast<InterfaceDrawing>(drone_list.back()));
    collision_list.push_back(std::dynamic_pointer_cast<InterfaceStageObject>(drone_list.back()));
    selectDrone(0);
  };
  /*!
  * \brief Method used for animating a drone
  * When a drone cannot land it corrects the flight to the moment when it is possible to land.
  */
  void animate(double length, double angle);
  /*!
  * \brief Method used for drawing approximate line of drone's course
  */
  void drawCourse(double length, double angle);
  /*!
  * \brief Method used for redrawing a line of drone's course, when it is impossible to land
  */
  void correctCourse(double length);
  /*!
  * \brief Method used for clearing a line of drone's course after landing
  */
  void clearCourse();
  /*!
  * \brief Method used for drawing all drawable objects on stage
  */
  void drawEverything() const;
  /*!
  * \brief Method used for adding landscape object
  */
  void addStageObject();
  /*!
  * \brief Method used for deleting landscape object of given id
  */
  void deleteStageObject(unsigned int id);
  /*!
  * \brief Method used for listing all stage objects and displaying centres of them
  */
  void showAllStageObjects() const;
  /*!
  * \brief Method used for adding drone object
  */
  void addDrone();
  /*!
  * \brief Method used for deleting drone object and if necessary changing the active one
  */
  void deleteDrone(unsigned int id);
  /*!
  * \brief Method used for displaying centres of the drones on stage
  */
  void showAllDrones() const;
  /*!
  * \brief Method used for choosing the active drone from the list
  */
  void selectDrone(unsigned int id);
  /*!
  * \brief Method used for displaying the centres of all objects with collision
  */
  void showAllCollisionObjects() const;

  /*!
  * \brief selected_drone setter
  */
  void setSelectedDrone(unsigned int id) {selected_drone = drone_list[id];};
};

#endif
