#ifndef HILL_HH
#define HILL_HH

#include <iostream>
#include <array>
#include <random>
#include <cmath>

#include "inter_drawing.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

#define EPSILON 10e-24
#define M_PI 3.14159265358979323846

/*!
 * \file
 * \brief File contains hill class
 */

/*!
* \brief Hill class used for defining hill features
*/
class Hill : public InterfaceDrawing, public InterfaceStageObject, public CoorSystem{
  /*!
  * \brief Array containing all of the hill shape corners
  */
  std::array<Vector<3>, 10> corners;
  /*!
  * \brief Variable containing hill height
  */
  double _height;
  /*!
  * \brief Variable containing minimal base radius size
  */
  double _size_min;
  /*!
  * \brief Variable containing maximal base radius size
  */
  double _size_max;
  /*!
  * \brief Variable containing generated maximal base radius size
  */
  double _max_radius;
  /*!
  * \brief Variable containing generated number of corners in the base
  */
  int _number_of_corners;
public:
  /*!
  * \brief Default constructor
  */
  Hill() = default;
  /*!
  * \brief Parametric constructor with parametres necessary for CoorSystem class and InterfaceDrawing class
  */
  Hill(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height, double size_min, double size_max, drawNS::APIopenGL3D* drafter):
	InterfaceDrawing(drafter, -1), CoorSystem(position, orientation, parent), _height(height), _size_min(size_min), _size_max(size_max){
    double x_coor, y_coor, theta, radius, angle_deg;
    _max_radius = size_min;
    std::random_device rd;
    std::mt19937 generator(rd());
    std::mt19937 generator_size(rd());
    std::uniform_int_distribution<int> corners_distribution(3,9);
    std::uniform_int_distribution<int> size_distribution(_size_min, _size_max);

    _number_of_corners = corners_distribution(generator);

    angle_deg = 360 / _number_of_corners;
    theta = M_PI * (angle_deg / 180);

    for (int i=0; i<_number_of_corners; i++){
      radius = size_distribution(generator_size);
      if(radius>_max_radius) _max_radius = radius;
      x_coor = cos(i*theta) * radius;
      y_coor = sin(i*theta) * radius;
      corners[i] = Vector<3>({x_coor, y_coor, 0});
    }
    corners[9] = Vector<3>({0, 0, _height});
  };

  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  virtual Vector<3> getcenter() override {return getPosition();};

  /*!
  * \brief Square brackets operator overloading used for getting corner of given index
  */
  const Vector<3> & operator [] (int indx) const;

  virtual bool isAbove(InterfaceDrone* drone) override;
  virtual double isLandingPossible(InterfaceDrone* drone) override {return -1;};

};

#endif
