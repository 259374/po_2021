#ifndef INTER_DRAWING_HH
#define INTER_DRAWING_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "OpenGL_API.hh"

/*!
 * \file
 * \brief File contains drawing interface abstract class
 */

/*!
* \brief This class is used for defining all objects drawing features for objects inheriting from this class
*/
class InterfaceDrawing{
protected:
  /*!
  * \brief Variable containing drawing api pointer
  */
  drawNS::APIopenGL3D* _drafter;
  /*!
  * \brief Variable containing drawn shape index
  */
  int _idx;
public:
  /*!
  * \brief Paramteric constructor assigning drafter to all drawable objects
  */
  InterfaceDrawing(drawNS::APIopenGL3D* drafter, unsigned int idx) : _drafter(drafter), _idx(idx) {};
  /*!
  * \brief Method used for drawing, every object has its own overrided method
  */
  virtual void draw() = 0;
  /*!
  * \brief Method used for erasing already drawn, every object has its own overrided method
  */
  virtual void clearShape() = 0;
  /*!
  * \brief Method used for getting an index of drawn shape, every object has its own overrided method
  */
  virtual int getIdx() = 0;
};

#endif
