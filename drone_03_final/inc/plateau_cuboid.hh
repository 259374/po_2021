#ifndef PLATEAU_CUBOID_HH
#define PLATEAU_CUBOID_HH

#include <iostream>
#include <array>

#include "inter_drone.hh"
#include "drone.hh"
#include "inter_stage_object.hh"
#include "cuboid.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

#define EPSILON 10e-24


/*!
 * \file
 * \brief File contains cuboid plateau shape class
 */

/*!
* \brief Cuboid plateau class used for defining cuboid plateau shape
*/
class PlateauCuboid : public InterfaceStageObject, public Cuboid{
public:
  /*!
  * \brief Default constructor
  */
  PlateauCuboid() = default;
  /*!
  * \brief Parametric constructor with parametres necessary for Cuboid class from which he inherits
  */
  PlateauCuboid(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height, double width, double depth, drawNS::APIopenGL3D* drafter):
	Cuboid(Vector<3> ({position[0],position[1],position[2]+0.5*height}), orientation, parent, height, width, depth, drafter) {};

  virtual bool isAbove(InterfaceDrone* drone) override;
  virtual double isLandingPossible(InterfaceDrone* drone) override;

  virtual Vector<3> getcenter() override {return getPosition();};

  /*!
  * \brief Getter used for getting corner of given index
  * Used for collision
  */
  Vector<3> getCorner(int idx) {return (*this)[idx];};
};

#endif
