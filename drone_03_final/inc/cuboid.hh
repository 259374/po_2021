#ifndef CUBOID_HH
#define CUBOID_HH

#include <iostream>
#include <array>

#include "inter_drawing.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

#define EPSILON 10e-24

/*!
 * \file
 * \brief File contains cuboid shape class
 */

/*!
* \brief Cuboid class used for defining cuboid shape
*/
class Cuboid : public InterfaceDrawing, public CoorSystem{
  /*!
  * \brief Array containing all of the cuboid corners
  */
  std::array<Vector<3>, 8> corners;
  /*!
  * \brief variable containing height of the cuboid
  */
  double _height;
  /*!
  * \brief variable containing width of the cuboid
  */
  double _width;
  /*!
  * \brief variable containing depth of the cuboid
  */
  double _depth;
public:
  /*!
  * \brief Default constructor
  */
  Cuboid() = default;
  /*!
  * \brief Parametric constructor with parametres necessary for CoorSystem class and InterfaceDrawing class
  */
  Cuboid(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height, double width, double depth, drawNS::APIopenGL3D* drafter):
	InterfaceDrawing(drafter, -1), CoorSystem(position, orientation, parent), _height(height), _width(width), _depth(depth){
    //Top
    corners[0] = Vector<3>({(_width/2),(_depth/2),(_height/2)});
    corners[1] = Vector<3>({(_width/2),(-1)*(_depth/2),(_height/2)});
    corners[2] = Vector<3>({(-1)*(_width/2),(-1)*(_depth/2),(_height/2)});
    corners[3] = Vector<3>({(-1)*(_width/2),(_depth/2),(_height/2)});

    //Bottom
    corners[4] = Vector<3>({(_width/2),(_depth/2),(-1)*(_height/2)});
    corners[5] = Vector<3>({(_width/2),(-1)*(_depth/2),(-1)*(_height/2)});
    corners[6] = Vector<3>({(-1)*(_width/2),(-1)*(_depth/2),(-1)*(_height/2)});
    corners[7] = Vector<3>({(-1)*(_width/2),(_depth/2),(-1)*(_height/2)});
  };
  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  /*!
  * \brief Depth variable getter
  */
  double getDepth() {return _depth;};
  /*!
  * \brief Width variable getter
  */
  double getWidth() {return _width;};
  /*!
  * \brief Height variable getter
  */
  double getHeight() {return _height;};

  /*!
  * \brief Square brackets operator overloading used for getting corner of given index
  */
  const Vector<3> & operator [] (int indx) const;
};

#endif
