#ifndef VECTOR_HH
#define VECTOR_HH

#include <iostream>
#include <array>

/*!
 * \file
 * \brief File contains templates of vector and rotation matrix class
 */

/*!
* \brief Vector class used for defining vectors with its arithmetics
*/
template <int SIZE>
class Vector{
private:
  /*!
  * \brief Array of double types containing values of vector
  */
  std::array<double, SIZE> coor;
  /*!
  * \brief Static variable containing existing number of vectors
  */
  static int exist_number;
  /*!
  * \brief Static variable containing created number of vectors
  */
  static int created_number;
public:
  //Constructors
  /*!
  * \brief Default constructor with counters
  */
  Vector() {exist_number++; created_number++;};
  /*!
  * \brief Paramteric constructor with counters
  */
  Vector(std::array<double, SIZE> arg){
    for (int i=0; i<SIZE; i++) coor[i] = arg[i];
    exist_number++; created_number++;
  };
  /*!
  * \brief Copying constructor with counters
  */
  Vector(const Vector & W) {
    for (int i=0; i<SIZE; i++) this->coor[i] = W.coor[i];
    exist_number++; created_number++;
  };

  //Arithmetics
  /*!
  * \brief Vector addition method
  */
  Vector<SIZE> operator + (const Vector<SIZE> & arg2) const;
  /*!
  * \brief Vector addition method with reference
  */
  Vector<SIZE> & operator += (const Vector<SIZE> & arg2);
  /*!
  * \brief Vector subtraction method
  */
  Vector<SIZE> operator - (const Vector<SIZE> & arg2) const;
  /*!
  * \brief Vector multiplication method
  */
  Vector<SIZE> operator * (double arg2) const;
  /*!
  * \brief Vector multiplication by value method
  */
  double operator * (const Vector<SIZE> & arg2) const;
  /*!
  * \brief Vector length method
  */
  double length() const;

  //Get/Set
  /*!
  * \brief Existing number of vectors getter method
  */
  static int get_exist_number() {return exist_number;};
  /*!
  * \brief Created number of vectors getter method
  */
  static int get_created_number() {return created_number;};
  double & operator[] (int indx);
  const double & operator [] (int indx) const;

  //Destructor
  /*!
  * \brief Deconstructor decrementing number of existing vectors
  */
  ~Vector() {exist_number--;};
};

template <int SIZE>
int Vector<SIZE>::exist_number = 0;

template <int SIZE>
int Vector<SIZE>::created_number = 0;

template <int SIZE>
/*!
* \brief Operator overloading for displaying a vector
*/
std::ostream & operator << (std::ostream & strm, const Vector<SIZE> & vec);

/*!
* \brief Operator overloading for vector input
*/
template <int SIZE>
std::istream & operator >> (std::istream & strm, Vector<SIZE> & vec);

/*!
* \brief Rotation matrix class used for defining rotation matrix with its arithmetics
*/
template <int SIZE>
class RotationMatrix{
private:
  /*!
  * \brief Array of vectors containing values in rows
  */
  std::array<Vector<SIZE>, SIZE> rows;
public:
  //Constructors
  /*!
  * \brief Default constructor
  */
  RotationMatrix() = default;
  /*!
  * \brief Parametric constructor
  */
  RotationMatrix(double _angle_deg, Vector<SIZE> _axis);

  //Arithmetics
  /*!
  * \brief Vector multiplication method
  */
  Vector<SIZE> operator * (const Vector<SIZE> & arg2) const;
  /*!
  * \brief Matricies multiplication method
  */
  RotationMatrix<SIZE> operator * (const RotationMatrix<SIZE> & arg2) const;
  /*!
  * \brief Matrix transposition method
  */
  RotationMatrix<SIZE> transpose() const;

  //Get/Set
  /*!
  * \brief Displaying value from matrix
  */
  const double operator () (int ind_row, int ind_col) const;
  /*!
  * \brief Displaying row from matrix
  */
  const Vector<SIZE> operator [] (int indx) const;
};


template <int SIZE>
RotationMatrix<SIZE>::RotationMatrix(double angle_deg, Vector<SIZE> axis){
  std::cerr << "Brak hiperobrotow" << std::endl;
}

/*!
* \brief Operator overloading for displaying a matrix
*/
template <int SIZE>
std::ostream & operator << (std::ostream & strm, const  RotationMatrix<SIZE> & rt_mtrx);

#endif
