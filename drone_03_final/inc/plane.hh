#ifndef PLANE_HH
#define PLANE_HH

#include <iostream>
#include <array>

#include "inter_drawing.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

#define EPSILON 10e-24

/*!
 * \file
 * \brief File contains plane class
 */

/*!
* \brief Plane class used for defining flat surface features
*/
class Plane : public InterfaceDrawing, public CoorSystem{
  /*!
  * \brief Array containing all of the plaen nodes
  */
  std::array<Vector<3>, 25> corners;
  /*!
  * \brief Variable containing plane positioning relative to Z-axis
  */
  double _height;
public:
  /*!
  * \brief Default constructor
  */
  Plane() = default;
  /*!
  * \brief Parametric constructor with parametres necessary for CoorSystem class and InterfaceDrawing class
  */
  Plane(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
        double height,  drawNS::APIopenGL3D* drafter):
	InterfaceDrawing(drafter, -1), CoorSystem(position, orientation, parent), _height(height){
    for (int i=0; i<5; ++i){
      for (int j=0; j<5; ++j){
        corners[5*i+j] = Vector<3>({-20+(double)(j*8),-20+(double)(i*8), height});
      }
    }
  };

  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  /*!
  * \brief Square brackets operator overloading used for getting node of given index
  */
  const Vector<3> & operator [] (int indx) const;

};

#endif
