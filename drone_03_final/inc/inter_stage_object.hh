#ifndef INTER_STAGE_OBJECT_HH
#define INTER_STAGE_OBJECT_HH

#include <iostream>
#include <array>

#include "inter_drone.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

#define EPSILON 10e-24

/*!
 * \file
 * \brief File contains stage object interface abstract class
 */

/*!
* \brief This class is used for defining all stage object features for objects inheriting from this class
*/
class InterfaceStageObject{
public:
  /*!
  * \brief Method used for checking if the drone given as a parametre has any object below him
  */
  virtual bool isAbove(InterfaceDrone* drone) = 0;
  /*!
  * \brief Method used for calculating landing distance
  * Returns -1 if landing is not possible
  * Returns positive value of double type which means the landing distance
  */
  virtual double isLandingPossible(InterfaceDrone* drone) = 0;
  /*!
  * \brief Method used for getting the vector of the center of the object
  */
  virtual Vector<3> getcenter() = 0;
};

#endif
