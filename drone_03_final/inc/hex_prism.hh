#ifndef HEX_PRISM
#define HEX_PRISM

#include <iostream>
#include <cmath>
#include <array>

#include "inter_drawing.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

#define EPSILON 10e-24

/*!
 * \file
 * \brief File contains hexagonal prism shape class
 */

/*!
* \brief Hexagonal prism class used for defining hexagonal prism shape
*/
class HexagonalPrism : public InterfaceDrawing, public CoorSystem{
  /*!
  * \brief Array containing all corners vectors
  */
  std::array<Vector<3>, 12> corners;
  /*!
  * \brief Height of a prism size
  */
  double _height;
  /*!
  * \brief Side of a prism size
  */
  double _side;
public:
  /*!
  * \brief Default constructor
  */
  HexagonalPrism() = default;
  /*!
  * \brief Parametric constructor with parametres necessary for CoorSystem class and InterfaceDrawing class
  */
  HexagonalPrism(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,
                 double height, double side, drawNS::APIopenGL3D* drafter):
	InterfaceDrawing(drafter, -1), CoorSystem(position, orientation, parent), _height(height), _side(side){
    double step = (_side)/sqrt(2);
    //Top
    corners[0] = Vector<3>({(_side/2 + step), (_side/2), (_height/2)});
    corners[1] = Vector<3>({0, (_side)/2 + step, (_height/2)});
    corners[2] = Vector<3>({(-1)*(_side/2 + step), (_side/2), (_height/2)});
    corners[3] = Vector<3>({(-1)*(_side/2 + step), (-1)*(_side/2), (_height/2)});
    corners[4] = Vector<3>({0, (-1)*((_side/2) + step), (_height/2)});
    corners[5] = Vector<3>({(_side/2 + step), (-1)*(_side/2), (_height/2)});

    //Bottom
    corners[6] = Vector<3>({(_side/2 + step), (_side/2), (-1)*(_height/2)});
    corners[7] = Vector<3>({0, (_side/2 + step), (-1)*(_height/2)});
    corners[8] = Vector<3>({(-1)*(_side/2 + step), (_side/2), (-1)*(_height/2)});
    corners[9] = Vector<3>({(-1)*(_side/2 + step), (-1)*(_side/2), (-1)*(_height/2)});
    corners[10] = Vector<3>({0, (-1)*((_side/2) + step), (-1)*(_height/2)});
    corners[11] = Vector<3>({(_side/2 + step), (-1)*(_side/2), (-1)*(_height/2)});
  };

  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  /*!
  * \brief Square brackets operator overloading used for getting corner of given index
  */
  const Vector<3> & operator [] (int indx) const;
};

#endif
