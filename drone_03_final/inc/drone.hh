#ifndef DRONE_HH
#define DRONE_HH

#include <iostream>
#include <cmath>
#include <array>
#include <thread>
#include <chrono>

#include "inter_drawing.hh"
#include "inter_drone.hh"
#include "inter_stage_object.hh"
#include "vector.hh"
#include "coor_system.hh"
#include "cuboid.hh"
#include "hex_prism.hh"
#include "OpenGL_API.hh"

#define EPSILON 10e-24
/*!
 * \file
 * \brief File contains drone structure class
 */

/*!
* \brief Drone class used for defining drone features
*/
class Drone : public InterfaceDrawing, public InterfaceStageObject, public InterfaceDrone, protected CoorSystem{
  /*!
  * \brief Cuboid type variable containing drone body
  */
  Cuboid body;
  /*!
  * \brief Array containing all of the drone's propellers
  */
  std::array<HexagonalPrism, 4> propellers;
  /*!
  * \brief Variable containing radius used for approximating its size in space
  */
  double radius;
public:
  /*!
  * \brief Default constructor
  */
  Drone() = default;
  /*!
  * \brief Parametric constructor with parametres necessary for CoorSystem, InterfaceDrawing, HexagonalPrism and Cuboid classes
  */
  Drone(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent,  drawNS::APIopenGL3D* drafter):
  InterfaceDrawing(drafter, -1),
  CoorSystem(Vector<3> ({position[0],position[1],position[2]+0.5}), orientation, parent),
  body(Vector<3>({0,0,0}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 1, 2, 4, drafter),
  propellers({
    HexagonalPrism(Vector<3> ({-1,2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter),
    HexagonalPrism(Vector<3> ({-1,-2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter),
    HexagonalPrism(Vector<3> ({1,2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter),
    HexagonalPrism(Vector<3> ({1,-2,0.5}), RotationMatrix<3>(0,Vector<3>({0,0,1})), this, 0.5, 0.75, drafter)
  }) {radius = sqrt((getBody().getDepth()/2) * (getBody().getDepth()/2) + (getBody().getWidth()/2)*(getBody().getWidth()/2));};

  virtual void goForward(double distance) override;
  virtual void goUp(double distance) override;
  virtual void goSide(double distance) override;
  virtual void turn(RotationMatrix<3> rot) override;

  virtual void turnPropellers() override;

  virtual int getIdx() override {return _idx;};

  virtual void draw() override;
  virtual void clearShape() override;

  virtual bool isAbove(InterfaceDrone* drone) override;
  virtual double isLandingPossible(InterfaceDrone* drone) override {return -1;};

  //Get
  virtual Vector<3> getcenter() override {return getPosition();};
  virtual Vector<3> dronegetcenter() override {return getPosition();};
  virtual RotationMatrix<3> dronegetorient() override {return getOrient();};
  virtual CoorSystem* dronegetparent() override {return getParent();};
  virtual double dronegetradius() override {return radius;};

  /*!
  * \brief Drone's body getter
  */
  Cuboid getBody() const {return body;};
  /*!
  * \brief Drone's propeller of given index getter
  */
  HexagonalPrism getPropeller(int which_one) const {return propellers[which_one];};
};

#endif
