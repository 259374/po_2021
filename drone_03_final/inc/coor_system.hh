#ifndef COOR_SYS
#define COOR_SYS

#include <iostream>
#include <array>
#include "OpenGL_API.hh"

#include "vector.hh"

/*!
 * \file
 * \brief File contains coordinate system class
 */

/*!
* \brief Coordinate system class used for object placement in 3D.
*
* Class implements all necessary methods to calculate or recalculate object in 3D space.\n
* Class has variables of types declared in vector and rotation matrix classes.\n
*
*/
class CoorSystem{
protected:
  /*!
   * \brief Pointer to the object of the same class used for ecalculating object position being a part of the other object inheriting from this class.
   *
   * This pointer points to the parent of the object.
   * If one objects is part of another then it inherits from it the orientation and center.
   */
  CoorSystem *_parent;
  /*!
   * \brief Variable of Vector<3> type - center of the inner coordinate system.
   */
  Vector<3> _center;
  /*!
   * \brief Variable of RotationMatrix<3> type - orientation of the inner coordinate system.
   */
  RotationMatrix<3> _orientation;
public:
  //Constructors
  /*!
   * \brief Default constructor
   */
  CoorSystem() = default;
  /*!
   * \brief Constructor with given parametres
   */
  CoorSystem(Vector<3> position, RotationMatrix<3> orientation, CoorSystem *parent = nullptr) :
  _parent(parent), _center(position), _orientation(orientation) {};

  /*!
   * \brief Moving center by given vectors
   */
  void move(Vector<3> v) {_center = _center + v;};
  /*!
   * \brief Rotating rotation matrix by given matrix
   */
  void rotate(RotationMatrix<3> mtrx) {_orientation = mtrx * _orientation;};
  /*!
   * \brief Recalculating given point by the orientation and placement of coordinate system
   */
  Vector<3> calculate_point(Vector<3> local_point) const;
  /*!
   * \brief Recalculating one coordinate system to the other
   */
  CoorSystem calculate_from_parent(const CoorSystem & parent);

  /*!
   * \brief Center position getter
   */
  Vector<3> getPosition() const {return _center;};
  /*!
   * \brief Coordinate system orientation getter
   */
  RotationMatrix<3> getOrient() const {return _orientation;};
  /*!
   * \brief Coordinate system parent getter
   */
  CoorSystem* getParent() const {return _parent;};
};

drawNS::Point3D convert (Vector<3> vec);

#endif
