#ifndef INTER_DRONE_HH
#define INTER_DRONE_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "coor_system.hh"
#include "OpenGL_API.hh"

/*!
 * \file
 * \brief File contains drone interface abstract class
 */

/*!
* \brief This class is used for defining all drone features for objects inheriting from this class
*/
class InterfaceDrone{
public:
  /*!
  * \brief Method used for calculating new vector after forward movement
  */
  virtual void goForward(double distance) = 0;
  /*!
  * \brief Method used for calculating new vector after upward movement
  */
  virtual void goUp(double distance) = 0;
  /*!
  * \brief Method used for calculating new vector after side movement
  * Unused
  */
  virtual void goSide(double distance) = 0;
  /*!
  * \brief Method used for calculating new vectors after drone rotation
  */
  virtual void turn(RotationMatrix<3> rot) = 0;
  /*!
  * \brief Method used for calculating new vectors after propeller rotation
  */
  virtual void turnPropellers() = 0;
  /*!
  * \brief Method used for getting the center of the drone
  */
  virtual Vector<3> dronegetcenter() = 0;
  /*!
  * \brief Method used for getting the rotation of the drone
  */
  virtual RotationMatrix<3> dronegetorient() = 0;
  /*!
  * \brief Method used for getting drone's coordinate system parent
  * Used for calculating line of course
  */
  virtual CoorSystem* dronegetparent() = 0 ;
  /*!
  * \brief Method used for getting the radius of the drone
  */
  virtual double dronegetradius() = 0;
};

#endif
