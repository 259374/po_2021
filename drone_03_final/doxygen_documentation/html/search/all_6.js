var searchData=
[
  ['hex_5fprism_2ehh_56',['hex_prism.hh',['../hex__prism_8hh.html',1,'']]],
  ['hexagonalprism_57',['HexagonalPrism',['../classHexagonalPrism.html',1,'HexagonalPrism'],['../classHexagonalPrism.html#ae769049daa3fe4b552c0d2f62d21b8c9',1,'HexagonalPrism::HexagonalPrism()=default'],['../classHexagonalPrism.html#a5170a5180d2935d0a8cc19a514ba89f2',1,'HexagonalPrism::HexagonalPrism(Vector&lt; 3 &gt; position, RotationMatrix&lt; 3 &gt; orientation, CoorSystem *parent, double height, double side, drawNS::APIopenGL3D *drafter)']]],
  ['hill_58',['Hill',['../classHill.html',1,'Hill'],['../classHill.html#a5be8138534ceab4844755c11f9368830',1,'Hill::Hill()=default'],['../classHill.html#afaf4a95ffda7fc689c6ba05af78c3234',1,'Hill::Hill(Vector&lt; 3 &gt; position, RotationMatrix&lt; 3 &gt; orientation, CoorSystem *parent, double height, double size_min, double size_max, drawNS::APIopenGL3D *drafter)']]],
  ['hill_2ehh_59',['hill.hh',['../hill_8hh.html',1,'']]]
];
