var searchData=
[
  ['redraw_202',['redraw',['../classdrawNS_1_1Draw3DAPI.html#ae88121104d2eeb8936f8dc1b68fc3bbf',1,'drawNS::Draw3DAPI::redraw()'],['../classdrawNS_1_1APIopenGL3D.html#ae51c8eb3c09dc53c54514e8ec355506d',1,'drawNS::APIopenGL3D::redraw()']]],
  ['rotate_203',['rotate',['../classCoorSystem.html#a22f9765829ff8b017a841b462d1cd688',1,'CoorSystem']]],
  ['rotationmatrix_204',['RotationMatrix',['../classRotationMatrix.html#a4cffe9c2b41773b8051a32d02568f456',1,'RotationMatrix::RotationMatrix()=default'],['../classRotationMatrix.html#abcb7cd4734833e0bc62e99d1507ac705',1,'RotationMatrix::RotationMatrix(double _angle_deg, Vector&lt; SIZE &gt; _axis)']]]
];
