#ifndef VECTOR_HH
#define VECTOR_HH

#include <iostream>

#define COOR_NUM 2

class Vector2D{
private:
  double coor[COOR_NUM];
public:
  //Constructors
  Vector2D(double _x, double _y) {coor[0]=_x; coor[1]=_y;};
  Vector2D() = default;

  //Arithmetics
  Vector2D operator + (const Vector2D & arg2) const;
  Vector2D & operator += (const Vector2D & arg2);
  Vector2D operator - (const Vector2D & arg2) const;
  Vector2D operator * (double arg2) const;
  double operator * (const Vector2D & arg2) const;
  double length() const;

  //Get/Set
  const double & operator [] (int indx) const;
  double & operator[] (int indx);
};

std::ostream & operator << (std::ostream & strm, const Vector2D & vec);
std::istream & operator >> (std::istream & strm, Vector2D & vec);

class RotationMatrix2D{
private:
  double angle_deg;
public:
  //Constructors
  RotationMatrix2D() = default;
  RotationMatrix2D(double _angle_deg) {angle_deg = _angle_deg;};

  //Arithmetics
  Vector2D operator * (const Vector2D & arg2) const;
  RotationMatrix2D operator * (const RotationMatrix2D & arg2) const;

  //Get/Set
  const double operator () (int indx1, int indx2) const;
};

std::ostream & operator << (std::ostream & strm, const  RotationMatrix2D & rm2d);

#endif
