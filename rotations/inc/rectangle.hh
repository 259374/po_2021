#ifndef RECTANGLE_HH
#define RECTANGLE_HH

#include <iostream>
#include <array>

#include "vector.hh"
#include "../Dr2D_gnuplot_api.hh"

class rectangle{
  std::array<Vector2D, 4> corners;
  int idx; //for drawing
public:
  rectangle() = default;
  rectangle(Vector2D _left_top, Vector2D _left_bot,
            Vector2D _right_top, Vector2D _right_bot){
              corners[0] = _left_top; corners[1] = _left_bot;
              corners[2] = _right_top; corners[3] = _right_bot;
            };
  void rotate(RotationMatrix2D rot);
  void move(Vector2D mv_vec);
  bool isRectangle();
  void draw(drawNS::Draw2DAPI *drawing);
  int getIdx(){return idx;};

  const Vector2D & operator [] (int indx) const;
};

drawNS::Point2D convert (Vector2D vec);

#endif
