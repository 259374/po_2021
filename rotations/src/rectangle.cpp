#include <iostream>
#include <iterator>


#include "vector.hh"
#include "rectangle.hh"
#include "../Dr2D_gnuplot_api.hh"

#define EPSILON 10e-20

void rectangle::rotate(RotationMatrix2D rot){
  this->corners[0] = rot * this->corners[0];
  this->corners[1] = rot * this->corners[1];
  this->corners[2] = rot * this->corners[2];
  this->corners[3] = rot * this->corners[3];
}

void rectangle::move(Vector2D mv_vec){
  this->corners[0] = this->corners[0] + mv_vec;
  this->corners[1] = this->corners[1] + mv_vec;
  this->corners[2] = this->corners[2] + mv_vec;
  this->corners[3] = this->corners[3] + mv_vec;
}

bool rectangle::isRectangle(){
  if (((this->corners[0] - this->corners[1]).length()
    - (this->corners[2] - this->corners[3]).length() < EPSILON)
    && ((this->corners[0] - this->corners[2]).length()
    - (this->corners[1] - this->corners[3]).length() < EPSILON)){
      if (((this->corners[0] - this->corners[1])
        * (this->corners[2] - this->corners[3]) < EPSILON)){
          return true;
        }
      }
  return false;
}

drawNS::Point2D convert (Vector2D vec){
  drawNS::Point2D outcome(vec[0],vec[1]);
  return outcome;
}

const Vector2D & rectangle::operator [] (int indx) const{
  if (indx < 0 || indx > 3){
    std::cerr << "Poza pamiecia" << std::endl;
    exit(0);
  }
  return corners[indx];
}


void rectangle::draw(drawNS::Draw2DAPI *drafter){
  std::vector<drawNS::Point2D> vtmp;

  vtmp.push_back(convert((*this)[0]));
  vtmp.push_back(convert((*this)[1]));
  vtmp.push_back(convert((*this)[2]));
  vtmp.push_back(convert((*this)[3]));
  vtmp.push_back(convert((*this)[0]));

  this->idx = drafter -> draw_polygonal_chain(vtmp, "red");
}
