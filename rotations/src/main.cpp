#include <iostream>


#include "vector.hh"
#include "rectangle.hh"
#include "../Dr2D_gnuplot_api.hh"

using std::vector;
using drawNS::Point2D;
using drawNS::APIGnuPlot2D;
using std::cout;
using std::cin;
using std::cerr;
using std::endl;

void wait4key() {
  do {
    cout << "\nPress a key to continue..." << endl;
  } while(cin.get() != '\n');
}

void rectSideInfo(rectangle rect){
  double a1,a2,b1,b2;
  a1 = (rect[0]-rect[3]).length();
  a2 = (rect[1]-rect[2]).length();
  b1 = (rect[0]-rect[1]).length();
  b2 = (rect[2]-rect[3]).length();

  if(!(a1==a2)){
    cout << ":( Dluzsze przeciwlegle boki nie sa sobie rowne: " << endl;
  }
  else{
    cout << ":) Dluzsze przeciwlegle boki sa sobie rowne: " << endl;
  }
  cout << a1 << endl;
  cout << a2 << endl;

  if(!(b1==b2)){
    cout << ":( Krotsze przeciwlegle boki nie sa sobie rowne: " << endl;
  }
  else{
    cout << ":) Krotsze przeciwlegle boki sa sobie rowne: " << endl;
  }

  cout << b1 << endl;
  cout << b2 << endl;
}

int main(){
  Vector2D mv_vec;
  Vector2D a(-4.0,2.0), b(-4.0,-2.0), c(4.0, -2.0), d(4.0,2.0);
  rectangle rect(a,b,c,d);
  RotationMatrix2D rot;
  double angle_degrees;
  double multiplier;
  std::string opt = "";

  drawNS::Draw2DAPI * drafter = new APIGnuPlot2D(-10,10,-10,10,0);
  rect.draw(drafter);

  cout.precision(20);

  rectSideInfo(rect);

  while(opt[0] != 'k'){
  cout << "Wybierz opcje (m - menu) : ";
  cin >> opt;
  switch(opt[0]){
    case 'w':{
      cout << endl << "Wspolrzedne wierzcholkow: " << endl;
      cout << "Lewy gorny: " << rect[0] << endl;
      cout << "Lewy dolny: " << rect[1] << endl;
      cout << "Prawy dolny: " << rect[2] << endl;
      cout << "Prawy gorny: " << rect[3] << endl;
    }
    break;
    case 'm':{
      cout << "\no - obrot o zadany kat" << endl;
      cout << "p - przesuniecie o zadany wektor" << endl;
      cout << "m - wyswietl menu " << endl;
      cout << "w - wyswietl wspolrzedne" << endl;
      cout << "k - koniec programu" << endl;
    }
    break;
    case 'p':{
      cout << endl << "Podaj wspolrzedne wektora przesuniecia : ";
      cin >> mv_vec;
      drafter->erase_shape(rect.getIdx());
      rect.move(mv_vec);
      rect.draw(drafter);
    }
    break;
    case 'o':{
      drafter->erase_shape(rect.getIdx());
      cout << endl << "Podaj kat obrotu (w stopniach) : ";
      cin >> angle_degrees;
      cout << "Podaj ilosc powtorzen operacji : ";
      cin >> multiplier;
      angle_degrees *= multiplier;
      rot = RotationMatrix2D(angle_degrees);
      rect.rotate(rot);
      rect.draw(drafter);
      rectSideInfo(rect);
    }
    break;
    case 'k': break;
    default:
      cout << "Wybierz jedna z opcji z menu" << endl;
    break;

  }
  wait4key();
  }

  delete drafter;
}
