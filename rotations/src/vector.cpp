#include <iostream>
#include <cmath>

#include "vector.hh"

#define COOR_NUM 2
#define M_PI 3.14159265358979323846

using std::cerr;
using std::endl;

Vector2D Vector2D::operator + (const Vector2D & arg2) const{
  Vector2D outcome;
  outcome.coor[0] = (*this)[0] + arg2.coor[0];
  outcome.coor[1] = (*this)[1] + arg2.coor[1];
  return outcome;
}

Vector2D & Vector2D::operator += (const Vector2D & arg2){
  (*this)[0] = (*this)[0] + arg2.coor[0];
  (*this)[1] = (*this)[1] + arg2.coor[1];
  return *this;
}

Vector2D Vector2D::operator - (const Vector2D & arg2) const{
  Vector2D outcome;
  outcome.coor[0] = (*this)[0] - arg2.coor[0];
  outcome.coor[1] = (*this)[1] - arg2.coor[1];
  return outcome;
}

Vector2D Vector2D::operator * (double arg2) const{
  Vector2D outcome;
  outcome[0] = (*this)[0] * arg2;
  outcome[1] = (*this)[1] * arg2;
  return outcome;
}

double Vector2D::operator * (const Vector2D & arg2) const{
  return (*this)[0]*arg2.coor[0] + (*this)[1]*arg2.coor[1];
}

double Vector2D::length() const{
  return sqrt((*this)[0]*(*this)[0] + (*this)[1]*(*this)[1]);
}

const double & Vector2D::operator [] (int indx) const{
  return coor[indx];
}

double & Vector2D::operator[] (int indx){
  if (indx < 0 || indx > COOR_NUM-1){
    cerr << "Poza pamiecia" << endl;
    exit(0);
  }
  return coor[indx];
}

std::ostream & operator << (std::ostream & strm, const Vector2D & vec){
  strm  << vec[0] << " " << vec[1];
  return strm;
}
std::istream & operator >> (std::istream & strm, Vector2D & vec){
  double numtmp;
  strm >> numtmp;
  vec[0] = numtmp;
  strm >> numtmp;
  vec[1] = numtmp;
  return strm;
}

Vector2D RotationMatrix2D::operator * (const Vector2D & arg2) const{
  Vector2D outcome;
  double x;
  x = 2 * M_PI * (angle_deg / 360);
  outcome[0] = arg2[0]*cos(x) - arg2[1]*sin(x);
  outcome[1] = arg2[0]*sin(x) + arg2[1]*cos(x);
  return outcome;
}

RotationMatrix2D RotationMatrix2D::operator * (const RotationMatrix2D & arg2) const{
  RotationMatrix2D outcome;
  outcome.angle_deg = this->angle_deg + arg2.angle_deg;
  return outcome;
}

const double RotationMatrix2D::operator() (int indx1, int indx2) const{
  double x;
  double outcome;
  x = 2 * M_PI * (angle_deg / 360);
  if (indx1 < 0 || indx1 > COOR_NUM-1){
    cerr << "Poza pamiecia" << endl;
    exit(0);
  }
  if (indx2 < 0 || indx2 > COOR_NUM-1){
    cerr << "Poza pamiecia" << endl;
    exit(0);
  }
  if (indx1+indx2 == 1){
    if(indx2){
      outcome = sin(x)*(-1);
      return outcome;
    }
    outcome = sin(x);
    return outcome;
  }
  outcome = cos(x);
  return outcome;
}

std::ostream & operator << (std::ostream & strm, const  RotationMatrix2D & rm2d){
  strm << '|' << rm2d(0,0) << "  " << rm2d(0,1) << '|' << endl << '|' << rm2d(1,0) << "  " << rm2d(1,1) << '|' << endl;
  return strm;
}
