#include <iostream>
#include <cmath>

#include "complex_number.hh"

Cnumber Cnumber::operator + (Cnumber num2) const{
  Cnumber outcome;
  outcome.re = re + num2.re;
  outcome.im = im + num2.im;
  return outcome;
}
Cnumber Cnumber::operator - (Cnumber num2) const{
  Cnumber outcome;
  outcome.re = re - num2.re;
  outcome.im = im - num2.im;
  return outcome;
}
Cnumber Cnumber::operator * (Cnumber num2) const{
  Cnumber outcome;
  outcome.re = (re * num2.re) - (im* num2.im);
  outcome.im = (im * num2.re) + (re * num2.im);
  return outcome;
}
Cnumber Cnumber::conjugate() const{
  Cnumber outcome;
  outcome.re = re;
  outcome.im = im * (-1);
  return outcome;
}
double Cnumber::modulus(){
  return sqrt(re * re + im * im);
}
Cnumber Cnumber::operator / (Cnumber num2) const{
  Cnumber outcome;
  outcome = (*this * num2.conjugate()) / pow(num2.modulus(), 2);
  return outcome;
}
Cnumber Cnumber::operator / (double divider) const{
  Cnumber outcome;
  outcome.re = re / divider;
  outcome.im = im / divider;
  return outcome;
}
Cnumber Cexpression::solve() const{
  Cnumber outcome;
  switch(Op){
    case Op_Add: outcome = num1 + num2;
    break;
    case Op_Substr: outcome = num1 - num2;
    break;
    case Op_Mult: outcome = num1 * num2;
    break;
    case Op_Div: outcome = num1 / num2;
    break;
  }
  return outcome;
}
bool Cnumber::operator == (Cnumber num2) const{
  const double e = 10E-10;
  if (std::abs(re-num2.re) > e){
    return false;
  }
  if (std::abs(im-num2.im) > e){
    return false;
  }
  return true;
}
bool Cnumber::operator != (Cnumber num2)const{
  const double E = 10E-10;
  if (std::abs(re-num2.re) > E){
    return true;
  }
  if (std::abs(im-num2.im) > E){
    return true;
  }
  return false;
}

/*In/Out Stream Operators*/

std::ostream & operator << (std::ostream & strm, const Cnumber & num){
  strm << "(" << num.getRe() << std::showpos << num.getIm() << std::noshowpos << "i)";
  return strm;
}
std::istream & operator >> (std::istream & strm, Cnumber & num){
  char tmp;
  double numtmp;
  strm >> tmp;
  if (tmp != '('){
    strm.setstate(std::ios::failbit);
  }
  strm >> numtmp;
  num.setRe(numtmp);
  strm >> numtmp;
  num.setIm(numtmp);
  strm >> tmp;
  if (tmp != 'i'){
    strm.setstate(std::ios::failbit);
  }
  strm >> tmp;
  if (tmp != ')'){
    strm.setstate(std::ios::failbit);
  }
  return strm;
}

std::ostream & operator << (std::ostream & strm, const Operator & Op){
  switch (Op){
    case Op_Add: strm << '+';
    break;
    case Op_Substr: strm << '-';
    break;
    case Op_Mult: strm << '*';
    break;
    case Op_Div: strm << '/';
    break;
  }
  return strm;
}
std::istream & operator >> (std::istream & strm, Operator & Op){
  char symbol;
  strm >> symbol;
  switch(symbol){
    case '+': Op = Op_Add;
    break;
    case '-': Op = Op_Substr;
    break;
    case '*': Op = Op_Mult;
    break;
    case '/': Op = Op_Div;
    break;
    default: strm.setstate(std::ios::failbit);
    break;
  }
  return strm;
}

std::ostream & operator << (std::ostream & strm, const Cexpression & exp){
  strm << exp.getNum1() << ' ' << exp.getOp() << ' ' << exp.getNum2();
  return strm;
}
std::istream & operator >> (std::istream & strm, Cexpression & exp){
  Cnumber numtemp;
  Operator optemp;
  strm >> numtemp >> optemp;
  exp.setNum1(numtemp);
  exp.setOp(optemp);
  strm >> numtemp;
  exp.setNum2(numtemp);
  return strm;
}
