#include <iostream>
#include <cmath>

#include "complex_number.hh"
#include "test_database.hh"
#include "statistics.hh"

#define ATTEMPTS 3

using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::string;

int main(int argc, char *argv[]){

  testDatabase exampleTest;
  Cexpression question;
  Cnumber guess;
  Statistics stat;
  int numOfAtmp;
  bool isCorrect;
  string file_name;

  if (argc > 2){
    cerr << "Podano zbyt wiele argumentow wywolania programu" << endl;
    return (-1);
  }

  if (argc < 2){
    cerr << "Nie podano poziomu trudnosci testu. Wpisz ./[program] ./tests/['latwy.txt'/'trudny.txt']." << endl;
    return (-1);
  }

file_name = argv[1];
exampleTest = testDatabase(file_name);

  while (exampleTest.getNextQuestion(question)){
    numOfAtmp = ATTEMPTS;
    cout << "Oblicz: " << question << endl;
    while (numOfAtmp > 0){
      cin >> guess;
      if (!cin.good()){
        numOfAtmp--;
        cin.clear();
        cin.ignore(1000,'\n');
        cerr << "Blad przy wprowadzaniu odpowiedzi. Pozostalych prob: " << numOfAtmp << endl;
        if (numOfAtmp > 0) cout << "Podaj wynik ponownie: ";
      }
      else{
        numOfAtmp = -1;
      }
    }

    if(question.solve() != guess) numOfAtmp = 0;

    if (numOfAtmp < 0) isCorrect = true;
    else isCorrect = false;

    stat.addAnswer(isCorrect);
    //cout << "Rozwiazanie: " << solve(question) << endl;
  }
  cout << stat;

}
