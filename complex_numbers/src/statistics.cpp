#include <iostream>

#include "statistics.hh"
#include "complex_number.hh"

void Statistics::addAnswer(bool isCorrect){
  this->setNumberOfQuestions(this->getNumberOfQuestions() +1);
  if (isCorrect){
    this->setCorrectAnswers(this->getCorrectAnswers() +1);
    std::cout << "Odpowiedz prawidlowa" << std::endl;
  }
  else{
    std::cout << "Odpowiedz nieprawidlowa" << std::endl;
  }
}

std::ostream & operator << (std::ostream & strm, const Statistics & stat){
  std::cout << "Poprawnych " << stat.getCorrectAnswers() << " odpowiedzi na " << stat.getNumberOfQuestions() << " pytan." << std::endl;
  return strm;
}
