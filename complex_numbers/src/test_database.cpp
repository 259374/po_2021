#include <iostream>
#include <cstring>
#include <cassert>

#include "test_database.hh"

using std::cout;
using std::cerr;
using std::endl;

testDatabase::testDatabase (std::string file_name){
  openFile(file_name);
}

bool testDatabase::openFile (std::string file_name){
  strm_database_file.open(file_name, std::fstream::in);
  if(!strm_database_file.good()){
    strm_database_file.setstate(std::ios::failbit);
    cerr << "Nie udalo sie otworzyc pliku" << endl;
    return false;
  }
  return true;
}

bool testDatabase::getNextQuestion (Cexpression & exp){
  strm_database_file >> exp;
  if (!strm_database_file.good()){
    strm_database_file.clear();
    strm_database_file.ignore(1000,'\n');
    return false;
  }
  return true;
}
