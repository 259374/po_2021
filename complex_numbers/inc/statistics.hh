#ifndef STATISTICS_HH
#define STATISTICS_HH

#include "complex_number.hh"

class Statistics{
private:
  unsigned int correctAnswers;
  unsigned int numberOfQuestions;
public:
  void addAnswer(bool isCorrect);

  /*Necessary for stream functions*/
  unsigned int getCorrectAnswers() const {return correctAnswers;};
  unsigned int getNumberOfQuestions() const {return numberOfQuestions;};
  void setCorrectAnswers(unsigned int _correct) {correctAnswers = _correct;};
  void setNumberOfQuestions(unsigned int _numberOfQuestions) {numberOfQuestions = _numberOfQuestions;};

  /*Constructor*/
  Statistics() : correctAnswers(0), numberOfQuestions(0) {}
};

std::ostream & operator << (std::ostream & strm, const Statistics & stat);

#endif
