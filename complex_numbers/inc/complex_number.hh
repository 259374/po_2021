#ifndef COMPLEXNUMBER_HH
#define COMPLEXNUMBER_HH

#include <iostream>

/**********************************************************
  This file contains classes of:
  - complex number 'Cnumber'
  - complex number expression 'Cexpression'
**********************************************************/

enum Operator { Op_Add, Op_Substr, Op_Mult, Op_Div };

class Cnumber{
private:
  double re;
  double im;
public:
  /*Arithmetics*/
  Cnumber operator + (Cnumber num2) const;
  Cnumber operator - (Cnumber num2) const;
  Cnumber operator * (Cnumber num2) const;
  Cnumber conjugate() const;
  double modulus();
  Cnumber operator / (Cnumber num2) const;
  Cnumber operator / (double divider) const;
  bool operator == (Cnumber num2) const;
  bool operator != (Cnumber num2) const;

  /*Necessary for stream functions*/
  double getRe() const {return re;}
  double getIm() const {return im;}
  void setRe(double _re) {re = _re;}
  void setIm(double _im) {im = _im;}

  /*Constructors*/
  Cnumber (double _re, double _im) : re(_re), im(_im) {}
  Cnumber () = default;
};

class Cexpression {
private:
  Cnumber num1;
  Operator Op;
  Cnumber num2;
public:
  /*Arithmetics*/
  Cnumber solve() const;

  /*Necessary for stream functions*/
  Cnumber getNum1() const {return num1;};
  Cnumber getNum2() const {return num2;};
  Operator getOp() const {return Op;};
  void setNum1(Cnumber _num1) {num1 = _num1;};
  void setNum2(Cnumber _num2) {num2 = _num2;};
  void setOp(Operator _Op) {Op = _Op;};

  /*Constructor*/
  Cexpression (Cnumber _num1, Operator _Op, Cnumber _num2): num1(_num1), Op(_Op), num2(_num2) {}
  Cexpression () = default;
};

std::ostream & operator << (std::ostream & strm, const Cnumber & num);
std::istream & operator >> (std::istream & strm, Cnumber & num);

std::ostream & operator << (std::ostream & strm, const Operator & Op);
std::istream & operator >> (std::istream & strm, Operator & Op);

std::ostream & operator << (std::ostream & strm, const Cexpression & exp);
std::istream & operator >> (std::istream & strm, Cexpression & exp);

#endif
