#ifndef TESTDATABASE_HH
#define TESTDATABASE_HH

#include <fstream>
#include <iostream>

#include "complex_number.hh"

/*CZYTANIE Z PAMIECI*/

class testDatabase {
private:
  std::fstream strm_database_file;
public:
  bool getNextQuestion(Cexpression & exp);
  bool openFile(std::string file_name);
  testDatabase(std::string file_name);
  testDatabase() = default;
};

#endif
